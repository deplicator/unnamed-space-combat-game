unnamed-space-combat-game
=========================
A game, probably in space with some shooting.

See [task list][tasklist]. **It's important.**

Need help with your dev environment? [Go here][contributing].


----------------------------------------------------------------------------------------------------

Updating The Database
---------------------
All the static database objects start as files in `server/db/`. There is a folder for each type of
object; active modules, passive modules, powerups, ships, and weapons. When `gulp updatedb` is run,
the task manager starts a script that will look in each of these directories for objects and updates
the database with what it finds. _It first drops the collection_, then recreates everything.

Each directory represents a collection in the database. Each file in the directory can represent a
single object, or many objects. A single object is represented as a JSONobject and a group of
objects are represented as a JSONarray. Notibly, the difference is the first character of the file.
A `{` for JSONobject and `[` for JSONarray.

Here is an example of a single JSONobject.

    {
        "id": 101,
        "name": "Single Weapon Example1"
    }

And here is an example of a JSONarray with two items.

    [
        {
            "id": 101,
            "name": "Single Weapon Example2"
        },
        {
            "id": 102,
            "name": "Another Weapon Example"
        }
    ]

Notice there are two weapon modules with the same id. Only one will be imported to the database.
There is no assurance about which one will be imported (so just be sure id's are unique).

Invalid json files will be ignored, but noted in the console. Use [an online json validation
tool](http://jsonlint.com/) to fix these issues.

Files that don't end in .json are ignored, so we can use something like `weapon.json.exmple` as a
template for weapons. We don't have an official way of doing these json files, but my initial
thoughts are large things in a single file (like a file for each ship) and little things grouped
into a file (like maybe all level 1 passive modules). We can work that out as we go.


Some Ship Info
--------------
Five groups of ships, four named for their predominate color; blue, green, purple, red, and the last
one is called bonus.

Each group is divided into classes. There are five classes (although not all groups have all five).
From weakest to most powerful; frigate, cruiser, destroyer, battlecruiser, battleship.

There are several files for the ship images, after class they are broken down by size. Either large
(lg) or small (sm). Large is used for displaying on the website where small is for in game.

Finally there is a code at the end of each file name, this is for upgrades with visual effects (only
engines and thrusters for now). A ship with no upgrades will be 00. A ship with only the engine
upgraded will be 10, only thrusters is 01, and both engine and thrusters is 11.

    src/assets/ships/<group>/<class>-<size>-<code>.png

In each group folder there are some other files as well. The `*.pdn` files are paint.net originals.
`shapes.json` is the boundary outline (for in game physics) file for all the ship in this group. The
`.pes` is the PhysicsEditor project file used to create the shapes.json.

[tasklist]: https://bitbucket.org/deplicator/unnamed-space-combat-game/src/HEAD/tasklist.md
[contributing]:https://bitbucket.org/deplicator/unnamed-space-combat-game/src/HEAD/docs/contributing.md
