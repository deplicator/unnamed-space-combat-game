Contributing
============
There is a good chance some of this is out of date.

Directory Structure
-------------------
This section updated on February 19, 2016.

**.git** hidden directory for git, don't worry about it.

**bower_componets** ignored by git, directory for bower packages that gets populated when
`bower install` is run. The packages are defined in `bower.json`.

**client** ignored by git, this folder is deleted and recreated when node is started. Files from
`src` are copied to it, some are modified (like js files). When node starts this is the web root.

**data** ignored by git, this is where the database files are kept. If you get errors when running
`git add .` [this should help](http://stackoverflow.com/questions/6030530/git-ignore-not-working-in-a-directory).

**node_modules** ignored by git, directory for node packages that gets populated when `npm install`
is run. The packages are defined in `package.json`.

**server** this is where the server runs from, lots of code will go here.

**src** this is the source to create the client directory, lots of code will go here too.

**src/assets** Put pictures and sounds and whatever kind of media in here.

**src/css** Put stylesheets in here.

**src/js** Put JavaScript files in here. They will be concatenated and minified when copied to
`client`.

`.bowerrc` tells bower where to install bower packages.

`.gitignore` tells git what to ignore.

`.jshintrc` stores the jshint settings for gulp with it runs the jshint task.

`bower.json` tells bower what packages and versions to use.

`config-example.json` The example file to base your `config.json` file on. Gulp will automatically
copy and rename it to `config.json` if you don't already have a `config.json` file.

`config.json` Ignored by git because it has all our API keys. You will have to set this up manually.

`contributing.md` this file, duh.

`gulpfile.js` is where gulp tasks are defined. For example when you run `gulp` to start the server
this file defines what the default tasks are.

`log.txt` will only exist if you've ever run `gulp -log`. It will be overwritten every time you run
that command. This file is also ignored by git.

`package.json` tells npm what packages and versions to use.

`readme.md` is now general info.

`tasklist.md` tells us what needs to be worked on. If you find bugs you cannot deal with right now
this is a good place to write it down.


Packages We Use
---------------
### Node.js Packages
These are all server side packages found in the `node_modules` folder. This folder is excluded from
the repository because they take up a lot of space and are easy to get with `npm install`.

**express** is an http server framework. Node has an http server built in, but this one is easier to
use and expand.

**mongodb** is what makes the connection between Node.js and the Mongo server.

**socket.io** handles receiving and sending data through sockets on the server side.

**bower** is a package manager like npm, except for the client side. It's not necessary, but nice to
keep everything up to date across all our local environments and the server.

**gulp** and various gulp packages are for development only. Gulp is a tool used for automating
tasks. For example, when you run the node server, gulp can minify all your js files into one file.
Gulp is poorly named (imo) but a clever tool. [Check out
this](https://github.com/gulpjs/gulp/blob/master/docs/README.md#articles) for more.

**browser-sync** This is what restarts the browser when files are edited, it works with gulp.

### Bower packages
Client side package manager (See above).

**Phasor** the html5 game engine we're using, so far the only package but we'll have more.


Setting up Development Environment
----------------------------------
Dev setup is a one time thing. Once you are set up write some code. We should add a section about
workflow later.

Required software:

* Node.js 0.12.7
* MongoDB 3.0.6
* Google Chrome (for live reload and future controller support)

Recommended development tools:

* git bash (if on Windows)
* Atom or Sublime

### Install things
Download and install [Node.js](https://nodejs.org/en/download/). You will probably want the 64-bit
Windows Installer (.msi).

Download and install [MongoDB](https://www.mongodb.org/downloads). Choose **Windows 64-bit 2008
R2+**. Install with default options.

[Git Bash](https://git-for-windows.github.io/) is a command line tool for Windows that is
Linux-like. This will make using git, node, and mongodb easier.

A text editor of choice, I like [Atom](https://atom.io/) for it's git integration, but it does not
handle large files well. [Sublime](http://www.sublimetext.com/) is good at handling large files, but
it will ask you to purchase is every ~20 saves.
[Notepad++](https://notepad-plus-plus.org/download/v6.8.3.html) is also a an option on Windows.

### Do command line things
Open Git Bash and run this command to install gulp and bower globally. These are useful node
packages.

    npm install gulp bower -g

From whatever directory you want (and have full rights to) clone the git repository. _Pro tip:_ you
can paste in the Git Bash console by right clicking on the title bar, go to **Edit -> Paste**.

    git clone https://<your user name>@bitbucket.org/deplicator/unnamed-space-combat-game.git

Change to the directory created after the clone.

    cd unnamed-space-combat-game

Next you will need node packages specific to this project.

    npm install

Then you will need bower packages for this project.

    bower install

Bower is similar to npm except npm is for server side node packages and bower is for client side
packages. Bower will help us keep all the JavaScript libraries (jQuery, Phasor, Underscore) in
order. To see what packages npm and bower install, check out `package.json` and `bower.json`
respectively.

### Setup the database
MongoDB will need to be running for Node.js to make it's connection to the database. This is
particularly annoying on Windows. To run, you need to execute `mongod.exe` from an admin console.
Git Bash works well for this and you'll want to open a new instance of it (the added effect of
looking badass by running two consoles is nice too). The following is an example, you'll need to
edit the paths based on where you installed MongoDB and where you setup your dev environment.

    /c/Program\ Files/MongoDB/Server/3.0/bin/mongod.exe --dbpath "c:\Users\james\development\unnamed-space-combat-game\data"

Once the database is running, that terminal becomes useless. To shutdown it down go to a free
terminal, or open a new one and type something like.

    /c/Program\ Files/MongoDB/Server/3.0/bin/mongod.exe

This will put you in a mongo console. From in here you can shutdown the database server cleanly by
typing:

    use admin
    db.shutdownServer()

Clean shutdown is important and will have to be done to add files to a git commit.

### Install MongoDB as a service
I found how to make using MongoDB less annoying. It can be [installed as a
service](http://stackoverflow.com/a/7895724). Open a command prompt as an administrator (not a git
bash window: _windows key->search for "cmd"->right click->run as administrator_). Run a command like
this (change you paths for your machine):

    "c:\Program Files\MongoDB\Server\3.0\bin\mongod.exe"
    --dbpath "c:\Users\james\development\unnamed-space-combat-game\data"
    --logpath="c:\Users\james\development\unnamed-space-combat-game\data\log.txt"
    --install

Then go to `services.msc`, easiest way is _windows key->search for "services.msc"_. You'll see
MongoDB on the services list. You can right click on it to start. If you go to properties you can
choose a **Startup type**. I went with **Automatic (Delayed Start)**.

### Get to Work
At this point the development environment is installed and you can run.

To start server run `gulp` from Git Bash. The output should look something like this:

    $ gulp
    [12:34:07] Using gulpfile c:\Users\james\development\unnamed-space-combat-game\gulpfile.js
    [12:34:07] Starting 'scripts'...
    [12:34:07] Starting 'lint'...
    [12:34:07] Starting 'nodemon'...
    [12:34:07] Finished 'nodemon' after 3.89 ms
    [12:34:07] Starting 'browser-sync'...
    [12:34:07] Finished 'browser-sync' after 78 ms
    [12:34:07] Finished 'lint' after 238 ms
    [12:34:07] Finished 'scripts' after 259 ms
    [12:34:07] Starting 'build'...
    [12:34:07] Finished 'build' after 6.35 μs
    [12:34:07] Starting 'default'...
    [12:34:07] Finished 'default' after 12 μs
    [12:34:07] [nodemon] v1.4.1
    [12:34:07] [nodemon] to restart at any time, enter `rs`
    [12:34:07] [nodemon] watching: *.*
    [12:34:07] [nodemon] starting `node server.js`
    { address: '::', family: 'IPv6', port: 3000 }
    HTTP Server running at http://:::3000
    Connected correctly to server.
    [BS] Proxying: http://localhost:3000
    [BS] Access URLs:
     ---------------------------------------
           Local: http://localhost:1337
        External: http://192.168.25.101:1337
     ---------------------------------------
              UI: http://localhost:3001
     UI External: http://192.168.25.101:3001
     ---------------------------------------
    [BS] Watching files...
             file requested : css/main.css
             file requested : bower_components/phaser-official/build/phaser.min.js
             file requested : js/boot.js
             file requested : js/preloader.js
             file requested : js/menu.js
             file requested : js/game.js
             file requested : js/main.js
             file requested : assets/preloader.gif
             file requested : favicon.ico
             file requested : assets/player.png
             file requested : assets/minecraftia.png
             file requested : assets/minecraftia.xml


And you can navigate to `http://localhost:1337` in Chrome. If it doesn't show up right away, give it
a minute. Going to `http://localhost:3001` will give BrowserSync information and options (it's cool
but we don't really need it right now).

If you get errors about failing to `GET` files from the server in the console, try right clicking
and holding the refresh button in Chrome then choose **Empty Cache and Hard Reload**.

A note git work flow, you have to stop MongoDB to run git add. There is probably a better way to
handle this, I'll look into it someday.
