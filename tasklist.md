The Task List
=============
Web Application
---------------
### Backend
- ~~PRIORITY we need to write back to the database!~~ -Bisoye
    - ~~Set up connection with database~~ -Bisoye
    - ~~Save new player gamer tag to database~~ -Bisoye
    - ~~Update the gamer tag session variable~~ -Bisoye
    - ~~Generic POST to update entire player object~~ -James

- Pass db stuff to all screens on front end (routes.js)
    - ~~chunk of code in routes to make object out of player session~~ handled by Passport -James
    - ~~chunk of code in routes to make object out of ships db~~ (use as example) -James
    - ~~chunk of code in routes to make object out of weapon modules db~~ -Anthony
    - ~~chunk of code in routes to make object out of active modules db~~ -James
    - ~~chunk of code in routes to make object out of passive modules db~~ -James
    - ~~chunk of code in routes to make object out of powerups db~~ -James
    - pass those objects to appropriate pages/cleanup

- ~~task manger: jshint back end js files~~ -James
- ~~Fix all the new jshint problems in backend~~ -James
- ~~update task manager to inject web js files correctly~~ -James
- HOLD: rename src folder client, rename client public (don't forget to update tasks and .gitignore)
- HOLD: rename game.js to use class convention of other files (Uscg.js)

### Front End
- ~~Need to separate web js files and game js files~~ -James
- ~~move html header to own file~~ -James

#### Message System
- a message system that is better than alerts
- message to let unregistered users know their progress will not be saved
- ~~browser message for unsupported browsers (so far Chrome only)~~ Jibaniya

#### Navigation Bar
- ~~show current credits~~ -Bisoye
- ~~credits should not be clickable~~ -Anthony
- ~~disable some nav items in game~~ -James
- ~~Play anyone button on all screens~~ -James
- ~~show currently selected ship~~ Anthony
- ~~quick ship select drop down~~ Anthony
- ~~volume controls~~ (switching pages mutes again, not an easy fix) -James
- logout in game should send forfeit to server
- BUG: Forfeit button doesn't remove player correctly
- BUG: nav bar too tall on narrow page width (doens't break to drop down menu ar the right width)
- ~~BUG: volume controls work in game, plays both songs~~ -James
- give unregistered player notice they're about to lose everything on logout (message system)
- link in footer to about page (see new About Screen section)


#### About Screen
- ~~add an about page~~ -Jibaniya
- ~~document where we got ship images (cc license)~~ -Jibaniya
- ~~document music source(s)~~ -Jibaniya
- ~~write in any other useful info (libraries used, particularly Phaser.io)~~ -Jibaniya
- ~~people to thank (school, Dr. Lodgher)~~ -Jibaniya
- ~~maybe put our names on it~~ -Jibaniya
- ~~we _need_ and email address on here per our requirements~~
- BUG: bottom cut off
- links to libraries
- pretty it up

#### Ship Selection Screen
- ~~show ship layouts from player object~~ -Anthony
- ~~PRIORITY: update selected ship on click~~ -James
- ~~PRIORITY: update PlayerObj with new selected ship~~ -James
- ~~PRIORITY: save PlayerObj back to db~~ -James
- ~~warn player if no weapon is loaded on selected ship before they enter the game~~ -James
- selected ship with arrow keys, maybe some kind of carousal
- ~~BUG: ship name changes does not update quick select~~
- practice mode is broke

#### Workshop Screen
- ~~show current ship layout from player object~~ -Anthony
- ~~show inventory from player object~~ -Anthony
- ~~BUG: showing weapons in db, not player inventory~~ -Anthony
- ~~BUG: Workshop Screen will not render on facebook section~~ -Anthony
- ~~sortable/filterable inventory view~~ Sortable by tabs -Anthony
- ~~PRIORITY: ability to upgrade "upgradeable" ship stats (thrust, armor, shields, etc..)~~ -Anthony
- ~~PRIORITY: show currently equipped weapon on ship~~  shows name of current weapon on ship -Anthony
- ~~PRIORITY: ability to swap out weapons (click and drag would be cool)~~ -Anthony
- ~~deal with other modules after weapons work~~ Passive works too -Anthony
- ~~BUG: upgrade buttons go away on reload (I think it has something to do with the hidden class)~~ -James
- ~~make credit deductions for upgrades~~ -James
- ~~show upgrade costs~~ -James
- BUG: Multiples of the same slot don't update other slots when swapping modules
- ~~BUG: ship name changes does not update quick select~~ fixed with callback on savePlayerObj() -James
- slot swap code could be combined
- show passive module increases to attributes when applicable

#### Store Screen
- ~~PRIORITY: show available items to purchase~~ -James
- ~~show a qty of player's current inventory for item's they already own~~ -James
- ~~sortable/filterable view of store items~~ -James
- ~~PRIORITY: make ship purchases~~ -James
- ~~PRIORITY: make item purchases~~ -James
- ~~PRIORITY: save PlayerObj to server on each purchase~~ -James
- ~~make credit deductions for purchases~~ -James
- ~~BUG: Filter is not quite right~~ -James
- ~~add item specific stats to each item~~ -James
- confirmation of purchase (message syste)
- sell not implemented
- search by name
- BUG: owned value not updated on purchase (but is correct after refresh)
- BUG: purchasing a ship does not make it available in quick select until refresh

#### Stats Screen
- ~~Show basic stats ~~ -Bisoye
- ~~Show more complex stats~~ -Bisoye
- pretty it up

#### Profile Screen
- ~~show facebook info on profile page~~ -Bisoye
- ~~change gamer tag (and save it to the database)~~ -Bisoye
- ~~let users press enter to save gamer tag~~ -James
- ~~validate gamertag input for empty string~~ -James
- ~~show gamer tag in input box~~ -James
- ~~update PlayerObj on change~~ -James
- ~~POST PlayerObj to server~~ -James
- ~~BUG: going to profile using "Just Play" gives error (no fb.name to get)~~ -James
- ~~PRIORITY: update gamer tag in nav bar on change~~ -James
- BUG: Signing in with Facebook from Profile page doesn't actually save current progress.
- set focus to input box after clicking edit gamer tag
- show friends who have registered with LiveSpace (this might require some changes to how we use FB)

#### Settings Screen
- ~~PRIORITY: include sleft and sright~~ -James
- ~~PRIORITY: keys should correspond with phaser.io key input~~ -Bisoye
- ~~update PlayerObj on key change~~ -Bisoye
- ~~get that info back to db~~ -Bisoye
- ~~BUG: no "press key to assign" message~~ -James
- ~~player should not be able to select same key twice~~ - Bisoye
- ~~BUG: can click on multiple keys to reassign before assigning the previous keys~~ - Bisoye
- ~~BUG: some key assignments weird/unaccounted for (enter, F-keys, number pad, ins/home/pageup/etc)~~ - Bisoye
- ~~BUG: add CONTROL to list of invalid keys (ctrl+w closes Chrome)~~ - Bisoye
- ~~message for invalid keys~~ - Bisoye
- ~~BUG: player has to use BACKSPACE before assigning new key~~ -Jibaniya
- ~~add gamepad support~~ -James
- user changeable gamepad mapping
- pretty it up


Game
----
#### Client/Server Comm
- ~~Ready button is there, add go back button~~ -James
- ~~kick unresponsive players from room~~ -James
- add player armor and shield to broadcast sent to other clients
- BUG: pinpoint random disconnect
- BUG: jittery movement
- ~~BUG: socket.io being inserted into DOM twice~~ -James
- ~~BUG: socket io error on game start~~ this is caused by live reload and not on production server -James

#### Controls
- ~~add strafe keys (strafe thrusters?)~~ added notes to plaer.js -James
- ~~Get control keys from player object~~ -James
- auto all stop would be cool

#### Player Ship
- ~~basic player ship object passed from each client to server~~ -James
- ~~figure out slots~~ made notes in player.js -James
- ~~limit acceleration and turning based on ship selected~~ -James
- ~~player ships need armor and shields~~ -James
- ~~shield regen~~ -James
- ~~hud to show armor and shields~~ -James
- ~~BUG: Players[s.id].update() sometimes undefined~~ -James
- ~~Add a state after a user dies to display stats~~ - Bisoye
- ~~add armor regen as a stat~~ -James
- ~~tweak ship attributes to be more granular~~ -James
- ~~handle more than a single weapon module per ship~~ -James
- shot start locations incorrect for single and dual weapons
- update ship json outlines of ship boundaries
- need to use groups for collision detection
- ships need mass (to know who wins when we ram into each other!)
- ships should do damage when they smack into each other
- ~~add gamerTag to ship class~~ - Bisoye
- HOLD: show weapon module sprite on ship
- HOLD: show other module sprites on ship

#### Death Screen
- ~~ BUG: stats incorrect on death screen (I think it's showing wrong player)~~ -Bisoye
- ~~ BUG: explosion is now gone ~~ -Bisoye
- ~~ need a couple second pause when player explodes before death screen shows ~~ - Bisoye
- BUG: top cut off
- death screen should have buttons to go directly back into arena or back to ship select
- let player know who killed them


#### Shooting
- ~~have the ship use weapon module details for shots~~ -James
- ~~send shot data to other clients in game~~ -James
- ~~account for damage done on shot hit~~ -James
- ~~bullets are not limited by range~~ -James
- ~~bullets from other clients should be the same class as your own bullets~~ -James
- ~~ move forward shot speed increases with ship speed ~~ Resolved - Anthony
- BUG: Shooting opponent's bullet hurts opponent
- BUG: Sometimes hit damage pop up doesn't go away
- Need to use groups for collision detection
- hit message should round
- hit message should show up for all players
- bullets should do something on hit (like explode)

#### Play Arena
- ~~obstacles~~ -James
- ~~wrap around~~ -James
- ~~more than one~~ -James
- ~~pick random arena on game start, let all the clients know which one~~ -James
- ~~Play anyone button needs to be Ready button~~ -James
- ~~Need to use groups for collision detection~~ groups used between ship and wall collisions -James
- ~~Bug: players can materialize in walls (need start locations for map)~~ -James
- ~~set start locations, 10 per arena (currently picking random spot)~~ crude but works -James
- refine credit value recieved for match
- collect in game stats to save for later
- ~~powerup spawns~~ -James
- ~~power up effects on collide~~ -James
- ~~some kind of powerup indicator~~ -James
- ~~BUG: powerups should spawn from server~~ -James
- BUG: double speed boost powerup is ridiculous
- BUG: The Pac arena is missing walls in lower right corner
- mute would be nice in game, that song gets old after a while

### Database Updates
- ~~Can we multiple objects per file too?~~ yes we can -James
- ~~restructure how we manage game object definition files~~ -James
- ~~Read each directory, add objects based on files found~~ -James
- ~~Creates duplicates, probably should check for that~~ duplicates removed by id but no grantee on order -James
- ~~Errors on invalid json.~~ ignores bad json files, tells you in console -James
- ~~Should exclude files that don't end with .json (so we can have examples that don't get loaded into db)~~ -James
- ~~Shouldn't error when no json files in folder~~ -James
- ~~Document how this works in readme~~ -James
- last update field based on last modified time of file





Monotonous Tasks
----------------
- ~~BUG: mystery number error when called from pages that don't have it (stats, profile, etc)~~ -James
- ~~setup wiki~~ -James
- add to the Wiki
- ~~tutorial, like those kinds you see on other websites~~ -James
- tutorials could be made better (simple json object in `src/js/web/tutorial.js`)

### Ship sprites and images: in order of importance
- passive item icons
- active item icons
- weapon item icons
- Level 1 ships
    - ~~Green Frigate variations~~ -James
    - ~~Green Frigate particle emitters~~ -James
    - ~~Blue Frigate variations~~ -James
    - ~~Blue Frigate particle emitters~~ -James
    - ~~Red Frigate variations~~ -James
    - ~~Red Frigate particle emitters~~ -James
    - ~~Purple Frigate variations~~ -James
    - ~~Purple Frigate particle emitters~~ -James
    - ~~Bonus ship graphics (no variations needed)~~ -James
    - ~~Bonus ship particle emitters~~ -James
- Level 2 ships
    - ~~Green Cruiser variations~~ -James
    - ~~Green Cruiser particle emitters~~ -James
    - Blue Cruiser variations
    - Blue Cruiser particle emitters
    - Red Cruiser variations
    - Red Cruiser particle emitters
    - Purple Cruiser variations
    - Purple Cruiser particle emitters
- all ship's shape file (at least level 1 and 2 ships)
- all Level 3 ships (destroyers) variations and thrusters
- all level 4 ships (battleships and battlecruisers) variations and thrusters

### Music
- Find some workshop Music
- Find some shopping Music
- Find two more in game Musics
- Music controls (because no website should have music without the option to mute clearly visible)


Would be Cool
-------------
- thorough test in other browsers
- json editor for game objects
