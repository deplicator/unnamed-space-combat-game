'use strict';

var gulp = require('gulp');                 // Taskmanager
var nodemon = require('gulp-nodemon');      // Node monitor.
var runSequence = require('run-sequence');  // For running tasks in specific order.
var exec = require('child_process').exec;   // Run external commands (like npm install).
var clean = require('del');                 // For deleting files and directories.
var jshint = require('gulp-jshint');        // Analize JavaScript files.
var concat = require('gulp-concat');        // Mashes files together.
var uglify = require('gulp-uglify');        // Minify files.
var inject = require('gulp-inject');        // Insert links to JS and CSS in HTML.
var bs = require('browser-sync').create();  // Start or refresh browser with server.
var config = undefined;                     // The config file get's loaded later.
var nodePath = '';
var npmPath = '';
var bowerPath = '';

// Run script to update database.
gulp.task('updatedb', ['config'], function(cb) {
    return exec(nodePath + 'node server/db/update.js', function(err, stdout, stderr) {
        console.log(stdout);
        //console.log(stderr);
        cb(err);
    });
});

// Rebuild package dependanceies.
gulp.task('npm-rebuild', ['config'], function(cb) {
    return exec(npmPath + 'npm rebuild', {maxBuffer: 1024 * 500}, function(err, stdout, stderr) {
        //console.log(stdout);
        //console.log(stderr);
        cb(err);
    });
});

// Update node packages.
gulp.task('npm-install', ['config'], function(cb) {
    return exec(npmPath + 'npm install', function(err, stdout, stderr) {
        //console.log(stdout);
        //console.log(stderr);
        cb(err);
    });
});

// Update bower packages.
gulp.task('bower-install', ['config'], function(cb) {
    return exec(bowerPath + 'bower install', function(err, stdout, stderr) {
        //console.log(stdout);
        //console.log(stderr);
        cb(err);
    });
});

// Check js files on server for atrocities.
gulp.task('jshint:server', function() {
    return gulp.src('server/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Check js files on client for atrocities.
gulp.task('jshint:client', function() {
    return gulp.src('src/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Delete everything inside the client directory.
gulp.task('clean', function() {
    return clean(['client/**', '!client',]);
});

// Copy local library files from bower_compontes to client.
gulp.task('copy:jquery', function() {
    var jqueryPath = 'bower_components/jquery/dist/';
    return gulp.src([jqueryPath + '/*'])
        .pipe(gulp.dest('client/libs/'));
});

// Copy jquery table plugin from bower_compontes to client.
gulp.task('copy:jquery-stupid-table', function() {
    return gulp.src(['bower_components/jquery-stupid-table/stupidtable.min.js'])
        .pipe(gulp.dest('client/libs/'));
});

// Copy jquery ui plugin from bower_compontes to client.
gulp.task('copy:jquery-ui', function() {
    return gulp.src(['bower_components/jquery-ui/jquery-ui.min.js'])
        .pipe(gulp.dest('client/libs/'));
});

// Copy bootstrap css from bower_compontes to client.
gulp.task('copy:bootstrap:css', function() {
    var bootstrapPath = 'bower_components/bootstrap/dist/';
    return gulp.src([bootstrapPath + 'css/*'])
        .pipe(gulp.dest('client/css/'));
});

// Copy bootstrap fonts from bower_compontes to client.
gulp.task('copy:bootstrap:fonts', function() {
    var bootstrapPath = 'bower_components/bootstrap/dist/';
    return gulp.src([bootstrapPath + 'fonts/*'])
        .pipe(gulp.dest('client/fonts/'));
});

// Copy bootstrap js from bower_compontes to client.
gulp.task('copy:bootstrap:js', function() {
    var bootstrapPath = 'bower_components/bootstrap/dist/';
    return gulp.src([bootstrapPath + 'js/*.min.js'])
        .pipe(gulp.dest('client/libs/'));
});

// Copy bootstrap-tour css from bower_compontes to client.
gulp.task('copy:bootstrap-tour:css', function() {
    var bootstrapPath = 'bower_components/bootstrap-tour/build/';
    return gulp.src([bootstrapPath + 'css/*'])
        .pipe(gulp.dest('client/css/'));
});

// Copy bootstrap-tour js from bower_compontes to client.
gulp.task('copy:bootstrap-tour:js', function() {
    var bootstrapPath = 'bower_components/bootstrap-tour/build/';
    return gulp.src([bootstrapPath + 'js/*'])
        .pipe(gulp.dest('client/libs/'));
});

// Call all bootstrap copy tasks.
gulp.task('copy:bootstrap', function() {
    return runSequence(['copy:bootstrap:css', 'copy:bootstrap:fonts', 'copy:bootstrap:js', 'copy:bootstrap-tour:css', 'copy:bootstrap-tour:js']);
});

// Copy font awesome css from bower_components to client.
gulp.task('copy:fontawesome:css', function() {
    var fontawesomePath = 'bower_components/font-awesome/';
    return gulp.src([fontawesomePath + 'css/*'])
        .pipe(gulp.dest('client/css/'));
});

// Copy font awesome fonts from bower_components to client.
gulp.task('copy:fontawesome:fonts', function() {
    var fontawesomePath = 'bower_components/font-awesome/';
    return gulp.src([fontawesomePath + 'fonts/*'])
        .pipe(gulp.dest('client/fonts/'));
});

// Call all font awesome copy tasks.
gulp.task('copy:fontawesome', function() {
    return runSequence(['copy:fontawesome:css', 'copy:fontawesome:fonts']);
});

// Copy phaser from bower_components to client.
gulp.task('copy:phaser', function() {
    var phaserPath = 'bower_components/phaser-official/';
    return gulp.src([phaserPath + 'build/*.js', phaserPath + 'build/*.map'])
        .pipe(gulp.dest('client/libs/'));
});

// Call all copy library tasks.
gulp.task('copy:libs', function() {
    return runSequence(['copy:jquery', 'copy:jquery-ui', 'copy:jquery-stupid-table', 'copy:bootstrap', 'copy:fontawesome', 'copy:phaser']);
});

// Copy client js files to public.
gulp.task('copy:js', function() {
    return gulp.src('src/js/**/*.js')
        .pipe(gulp.dest('client/scripts'));
});

// Mashes all the js/game files in client into one, then copies to public.
gulp.task('copyuglyGame:js', function() {
    return gulp.src('src/js/game/*.js')
        .pipe(uglify())
        .pipe(concat('game.min.js'))
        .pipe(gulp.dest('client/scripts/game'));
});

// Mashes all the js/web files in client into one, then copies to public.
gulp.task('copyuglyWeb:js', function() {
    return gulp.src('src/js/web/*.js')
        .pipe(uglify())
        .pipe(concat('web.min.js'))
        .pipe(gulp.dest('client/scripts/web'));
});

// Mashes all the js/root files in client into one, then copies to public.
gulp.task('copyRoot:js', function() {
    return gulp.src('src/js/socket.io.js')
        .pipe(gulp.dest('client/scripts'));
});

gulp.task('copyugly:js', function() {
    return runSequence(['copyRoot:js', 'copyuglyWeb:js', 'copyuglyGame:js']);
});

// Copy CSS files (might branch this out to use SASS).
gulp.task('copy:css', function() {
    return gulp.src('src/css/*.css')
        .pipe(gulp.dest('client/css'));
});

// Copy files that don't need to be altered to client
gulp.task('copy:rest', function() {
    return gulp.src(['src/*.*', 'src/assets/**/*'], {base: 'src'})
        .pipe(gulp.dest('client'));
});

// Inject js and css file paths into ejs files, run after files are copied to client.
gulp.task('inject', function () {
    return gulp.src('client/*.ejs')

        // Inject jQuery first.
        .pipe(inject(
            gulp.src(
                'client/libs/jquery.min*',
                {read: false}
            ),
            {relative: true, quiet: true, name: 'jquery'}
        ))

        // Inject socket.io second.
        .pipe(inject(
            gulp.src(
                'client/scripts/socket.io.js',
                {read: false}
            ),
            {relative: true, quiet: true, name: 'socket'}
        ))

        // All other library files.
        .pipe(inject(
            gulp.src(
                ['client/libs/*min.js', '!client/libs/jquery.min.js'],
                {read: false}
            ),
            {relative: true, quiet: true, name: 'libraries'}
        ))

        // All css files.
        .pipe(inject(
            gulp.src(
                ['client/css/*min.css','client/css/main.css'],
                {read: false}
            ),
            {relative: true, quiet: true, name: 'style'}
        ))

        // Custom web js files.
        .pipe(inject(
            gulp.src(
                'client/scripts/web/*.js',
                {read: false}
            ),
            {relative: true, quiet: true, name: 'web'}
        ))

        // Custom game js files.
        .pipe(inject(
            gulp.src(
                'client/scripts/game/*.js',
                {read: false}
            ),
            {relative: true, quiet: true, name: 'game'}
        ))

        .pipe(gulp.dest('client'));
});

// Refresh browser with a file changes in the dev environment.
gulp.task('browser-sync', ['nodemon'], function() {
    bs.init(null, {
        proxy: 'http://localhost:3000',
        files: ['src/*.*', 'src/**/*.*'],
        browser: 'google chrome',
        port: 1337,
        online: false,
        notify: false
    });
});

// Watches for file changes and restarts node (https://github.com/remy/nodemon).
var nodemonArgs = []; // empty unless -log flag is used 'gulp -log'
gulp.task('nodemon', function () {
    var started = false;
    return nodemon({
        script: 'server/start.js',
        args: nodemonArgs,
        ext: 'html js css ejs',
        ignore: ['.git/*', 'bower_compontes/*', 'client/*', 'data/*', 'node_modules/*', 'gulpfile.js']
    }).on('restart', function() {
        console.log('restarted');
        gulp.start('restart');
    }).on('start', function () {
        if (!started) {
            started = true;
        }
    });
});

// Check and assign config file.
gulp.task('config', function() {

    // Check for config file.
    var checkConfig = function() {
        try {
            require.resolve('./config.json');
            return true;
        } catch (e) {
            console.log(e.message);
            return false;
        }
    };

    // Setup config globals.
    if (checkConfig()) {
        if (config === undefined) {
            config = require('./config.json');
            console.log('\n    Environment: ', config.environment, '\n');
            if (config.paths !== undefined) {
                if (config.paths.node !== null) {
                    nodePath = config.paths.npm;
                }
                if (config.paths.npm !== null) {
                    npmPath = config.paths.npm;
                }
                if (config.paths.bower !== null) {
                    bowerPath = config.paths.npm;
                }
                if (config.paths.python !== null) {
                    process.env['PYTHON'] += config.paths.python;
                }
            }
        }
    } else {
        console.log('File config.json not found.');
        var fs = require('fs');
        fs.createReadStream('./config-example.json').on('end', function() {
            console.log('Creating config.json from config-example.json.');
            console.log('UPDATE config.json file for your environment and rerun gulp!');
        }).pipe(fs.createWriteStream('./config.json'));
        return;
    }
});

// What to do after nodemon restart.
gulp.task('restart', function() {
    return runSequence('clean', ['copy:libs', 'copy:js', 'copy:css', 'copy:rest'], 'inject');
});

// What happens when `gulp` is run from command line.
gulp.task('default', ['config'], function () {
    // Get command line aruguments.
    var args = process.argv.slice(2);

    // If one of the first two aruguments is -log, set log file arguments to pass to nodemon.
    if(args[0] === '-log' || args[1] === '-log') {
        nodemonArgs = ['2>&1', '|', 'tee', 'log.txt'];
    }

    // Build is different on production server.
    if (config.environment === 'Production') {
        runSequence(
            'npm-rebuild',
            'updatedb',
            'npm-install',
            'bower-install',
            'clean',
            ['copy:libs', 'copyugly:js', 'copy:css', 'copy:rest'],
            'inject',
            'nodemon'
        );

    // No fast option in production. Skips database update, npm rebuild, npm and bower installs.
    } else if(args[0] === '-fast' || args[1] === '-fast') {
        runSequence(
            'clean',
            ['copy:libs', 'copy:js', 'copy:css', 'copy:rest'],
            'inject',
            'browser-sync'
        );

    // Normal development build.
    } else {
        runSequence(
            'npm-rebuild',
            'updatedb',
            'npm-install',
            'bower-install',
            'clean',
            'jshint:server',
            'jshint:client',
            ['copy:libs', 'copy:js', 'copy:css', 'copy:rest'],
            'inject',
            'browser-sync'
        );
    }
});
