/* global require, module */
// Mongoose model for player.

var mongoose = require('mongoose');
var Moniker = require('moniker');

// Load starts dictionary for Moniker.
function stars() {
    return Moniker.read('./server/models/stars.txt');
}

// Create random gamer tag with stars and adjective dictonaries.
function randomGamerTag() {
    var names = Moniker.generator([Moniker.adjective, stars], {
      maxSize: undefined,
      encoding: 'utf-8',
      glue: ' '
    });
    return names.choose();
}

// Player model schema.
var playerSchema = mongoose.Schema({
    profile: {
        gamerTag: {type: String, default: randomGamerTag},

        // profile.facebook will be undefined for unregestered players, use this to differentiate.
        facebook: {
            id:    String,
            token: String,
            email: String,
            name:  String
        }
    },
    settings: {
        controls: {
            forward: {type: String, default: 'W'},      // Main engine.
            reverse: {type: String, default: 'S'},      // Reverse thrusters.
            left: {type: String, default: 'A'},         // Turn left: starboard bow and aft port thruster
            right: {type: String, default: 'D'},        // Turn right: port bow and aft starboard thruster
            sleft: {type: String, default: 'Q'},        // Port thrusters only.
            sright: {type: String, default: 'E'},       // Starboard thrusters only.
            fire: {type: String, default: 'SPACEBAR'},  // Fire main weapons.
            useActive: {type: String, default: 'F'}     // Use active module if available.
        }
    },
    inventory: {
        // Ships is an array of ship id's which represents ships purchased. This array's index
        //realtes to the shiplayout's key. There will always be a ship layout for every ship, even
        // if it is emypy. This array will only have elements removed when ships are sold.
        ships: {type: Array, default: [111, 121]},

        // The other inventory items work a little differnt than ships. These represent tihngs the
        // player ownes but is not in a ship layout. Moving items to a ship layout remove them from
        // these array's, and removing a module from a ship layout will add it back.
        weapons: {type: Array, default: [111, 121, 131]},
        active: {type: Array, default: []},
        passive: {type: Array, default: [101]}
    },

    // Some stats and currency.
    stats: {
        credit: {type: Number, default: 100000},
        damageAbsorbed: {type: Number, default: 0},
        hitsTakenArmor: {type: Number, default: 0},
        hitsTakenShield: {type: Number, default: 0},
        shotsFired: {type: Number, default: 0},
        shotsHit: {type: Number, default: 0},
        spinsSeen: {type: Number, default: 0},

        // Flag so we know if this user has taken the tours.
        toursTaken: {
            ships: {type: Boolean, default: false},
            workshop: {type: Boolean, default: false},
            store: {type: Boolean, default: false}
        }
    },

    // Selected ship uses keys from shipslayout.
    selectedShip: {type: Number, default: 0},

    // Ship layout would be programatically determined in the workshop, these represent what players
    // start with.
    shiplayouts: {
        /* The two basic start ships should be well balanced against each other since most players
         * will be running around in them.
         *          Green           Blue
         * armor    more            less
         * shiled   none            some
         * weapon   energy          kinetic
         */

        0: { // key is index from inventory.ships array.

            // shipId needed to get more info from database.
            shipId: {type: Number, default: 111},

            // Name should be changeable by player.
            name: {type: String, default: 'Green Frigate'},
            level:  {type: Number, default: 1},
            sprites: {
                ship: {
                    name: {type: String, default: 'gf00'},
                    shape: {type: String, default: 'frigate-sm-00'},
                }
            },

            // Thursters are arrays of particle emiters for each direction.
            thrusters: {
                forward: {type: Array, default: [
                    {
                        particle: 'green-particle',
                        x: 0,
                        y: 40,
                        width: 10,
                        lifespan: 250,
                        minParticleSpeed: {
                            x: -30,
                            y: 0
                        },
                        maxParticleSpeed: {
                            x: 30,
                            y: 100
                        },
                        minParticleScale: 0.5,
                        maxParticleScale: 1,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 1
                    }
                ]},
                reverse: {type: Array, default: [
                    {
                        particle: 'green-particle',
                        x: 0,
                        y: -25,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: -5,
                            y: -30
                        },
                        maxParticleSpeed: {
                            x: 5,
                            y: 0
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.5,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 1
                    },
                    {
                        particle: 'green-particle',
                        x: 7,
                        y: -46,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: -5,
                            y: -35
                        },
                        maxParticleSpeed: {
                            x: 5,
                            y: 0
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.5,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 1
                    },
                    {
                        particle: 'green-particle',
                        x: -7,
                        y: -46,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: -5,
                            y: -35
                        },
                        maxParticleSpeed: {
                            x: 5,
                            y: 0
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.5,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 1
                    }
                ]},
                portbow: {type: Array, default: [
                    {
                        particle: 'green-particle',
                        x: -22,
                        y: -26,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: -30,
                            y: -5
                        },
                        maxParticleSpeed: {
                            x: 0,
                            y: 5
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.5,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 1
                    }
                ]},
                portaft: {type: Array, default: [
                    {
                        particle: 'green-particle',
                        x: -13,
                        y: 29,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: -30,
                            y: -5
                        },
                        maxParticleSpeed: {
                            x: 0,
                            y: 5
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.5,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 1
                    }
                ]},
                starboardbow: {type: Array, default: [
                    {
                        particle: 'green-particle',
                        x: 22,
                        y: -26,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: 0,
                            y: -5
                        },
                        maxParticleSpeed: {
                            x: 30,
                            y: 5
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.5,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 1
                    }
                ]},
                starboardaft: {type: Array, default: [
                    {
                        particle: 'green-particle',
                        x: 13,
                        y: 29,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: 0,
                            y: -5
                        },
                        maxParticleSpeed: {
                            x: 30,
                            y: 5
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.5,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 1
                    }
                ]},
            },
            imagepath: {type: String, default: 'assets/ships/green/frigate-lg-00.png'},

            // Represents current values of upgradeable attributes.
            attributes: {
                acceleration: {type: Number, default: 8}, // These are the start value for this ship.
                thrust: {type: Number, default: 10},
                armor: {type: Number, default: 4},
                armorRegen: {type: Number, default: 0}, // Most ships will only get this as a powerup or module.
                shield: {type: Number, default: 0},
                shieldRegen: {type: Number, default: 0}
            },

            // Array of slots available and what's inside them; 0 means empty.
            slots: {type: Array, default: [
                {
                    'moduleType': 'weapon',
                    'moduleMaxLevel': 1,
                    'moduleId': 121
                },
                {
                    'moduleType': 'active',
                    'moduleMaxLevel': 1,
                    'moduleId': 0
                },
                {
                    'moduleType': 'passive',
                    'moduleMaxLevel': 2,
                    'moduleId': 0
                }
            ]},
            bonus: {type: Array, default: [
                {
                    'bonusType': 'range',
                    'modifier': 1.25
                }
            ]}
        },

        // Basic start ship Blue Frigate has less armor, but has shields. Uses an energy weapon.
        1: {
            shipId: {type: Number, default: 211},
            name: {type: String, default: 'Blue Frigate'},
            level:  {type: Number, default: 1},
            sprites: {
                ship: {
                    name: {type: String, default: 'bf00'},
                    shape: {type: String, default: 'frigate-sm-00'}
                }
            },
            thrusters: {
                forward: {type: Array, default: [
                    {
                        particle: 'blue-particle',
                        x: -10,
                        y: 45,
                        width: 5,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: -10,
                            y: 0
                        },
                        maxParticleSpeed: {
                            x: 10,
                            y: 75
                        },
                        minParticleScale: 0.5,
                        maxParticleScale: 1,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.4,
                        maxParticleAlpha: 1
                    },
                    {
                        particle: 'blue-particle',
                        x: 10,
                        y: 45,
                        width: 5,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: -10,
                            y: 0
                        },
                        maxParticleSpeed: {
                            x: 10,
                            y: 75
                        },
                        minParticleScale: 0.5,
                        maxParticleScale: 1,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.4,
                        maxParticleAlpha: 0.9
                    }
                ]},
                reverse: {type: Array, default: [
                    {
                        particle: 'blue-particle',
                        x: 0,
                        y: -50,
                        width: 1,
                        lifespan: 150,
                        minParticleSpeed: {
                            x: -5,
                            y: -50
                        },
                        maxParticleSpeed: {
                            x: 5,
                            y: 0
                        },
                        minParticleScale: 0.25,
                        maxParticleScale: 0.5,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.4,
                        maxParticleAlpha: 0.9
                    }
                ]},
                portbow: {type: Array, default: [
                    {
                        particle: 'blue-particle',
                        x: -22,
                        y: -36,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: -30,
                            y: -5
                        },
                        maxParticleSpeed: {
                            x: 0,
                            y: 5
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.25,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 0.9
                    }
                ]},
                portaft: {type: Array, default: [
                    {
                        particle: 'blue-particle',
                        x: -20,
                        y: 29,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: -30,
                            y: -5
                        },
                        maxParticleSpeed: {
                            x: 0,
                            y: 5
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.25,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 0.9
                    }
                ]},
                starboardbow: {type: Array, default: [
                    {
                        particle: 'blue-particle',
                        x: 22,
                        y: -36,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: 0,
                            y: -5
                        },
                        maxParticleSpeed: {
                            x: 30,
                            y: 5
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.25,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 0.9
                    }
                ]},
                starboardaft: {type: Array, default: [
                    {
                        particle: 'blue-particle',
                        x: 20,
                        y: 29,
                        width: 1,
                        lifespan: 200,
                        minParticleSpeed: {
                            x: 0,
                            y: -5
                        },
                        maxParticleSpeed: {
                            x: 30,
                            y: 5
                        },
                        minParticleScale: 0.1,
                        maxParticleScale: 0.25,
                        minRotation: -360,
                        maxRotation: 360,
                        minParticleAlpha: 0.5,
                        maxParticleAlpha: 0.9
                    }
                ]},
            },
            imagepath: {type: String, default: 'assets/ships/blue/frigate-lg-00.png'},
            attributes: {
                acceleration: {type: Number, default: 4},
                thrust: {type: Number, default: 8},
                armor: {type: Number, default: 3},
                armorRegen: {type: Number, default: 0},
                shield: {type: Number, default: 4},
                shieldRegen: {type: Number, default: 4}
            },
            slots: {type: Array, default: [
                {
                    'moduleType': 'weapon',
                    'moduleMaxLevel': 1,
                    'moduleId': 111
                },
                {
                    'moduleType': 'passive',
                    'moduleMaxLevel': 1,
                    'moduleId': 0
                }
            ]},
            bonus: {type: Array, default: [
                {
                    'bonusType': 'damage',
                    'damageType': 'kinetic',
                    'modifier': 1.1
                }
            ]}
        }
    }
});

// Create the model for players and expose it to app.
module.exports = mongoose.model('Player', playerSchema);
