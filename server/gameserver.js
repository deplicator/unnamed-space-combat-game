/* global module */
/* jslint bitwise: true */

function server(io) {

    /* Server Globals */
    var debug = true;       // Output info to console.
    var minPlayers = 2;     // Minimum number of clients to start the game in a room.
    var maxPlayers = 10;    // Maximum number of clients in a room..
    var clients = {};       // Object with client socket as key, room as value.
    var clientsTime = {};   // Object with client socket as key, last time we heard from them as value.
    var clientsData = {};   // Object with client socket as key, client selected ship data as a value.
    var games = [];         // Array of current games with at least 1 player.
    var arenas = [];        // Array of current games's selected arena.

    /* Helper Functions */
    // Every some seconds, if clientsTime too old, disconnect client.
    function kickDisconnected() {
        for(var each in clientsTime) {
            if(clientsTime.hasOwnProperty(each)) {
                var now = new Date();
                var when = clientsTime[each];
                // kick if last check in was more than 5 seconds ago
                if (now.getSeconds() - when.getSeconds() > 1) {
                    if (debug) { console.log('CLIENT, unresponsive:', each); }
                    io.sockets.connected[each].disconnect();
                }
            }
        }
    }
    setInterval(kickDisconnected, 3000); // check every 6 seconds

    // Return new game id, add it to games array.
    function makeGameId() {
        var newid;
        do {
            newid = (0 | Math.random()*9e6).toString(36).substring(0,4);
        }
        while (games.indexOf(newid) >= 0);
        games.push(newid);
        return newid;
    }

    // Start timer on room, index is the game and arena index of this room
    function startTimer(index) {
        // Let room know to spawn a powerup every 60 seconds.
        setInterval(function () {
            var room = games[index];
            var arena = arenas[index];
            var spawns = 0;
            //1: the cage has 4 powerup spwans
            //2: the pac has 5 powerup spawns
            //3: the classic has 8 powerup spawns
            if (arena === 1) { spawns = 4; }
            else if (arena === 2) { spawns = 5; }
            else if (arena === 3) { spawns = 8; }
            var spawnLoc = Math.floor(Math.random() * spawns);
            var whichPowerup = Math.floor(Math.random() * 3) + 1; //There are only 3.

            io.to(room).emit('clientUpdate', {
                isPowerup: true,
                spawnLoc: spawnLoc,
                whichPowerup: whichPowerup
            });
        }, 60000);
    }

    // Returns array of client socket's in room.
    function socketsInRoom(room) {
        if (io === undefined) {
            console.log('io is undefined :\\','e');
        }

        if (io.nsps['/'] === undefined) {
            console.log('/ namespace is undefined :\\','e');
        }

        if (io.nsps['/'].adapter === undefined) {
            console.log('adapter is undefined :\\','e');
        }

        if (io.nsps['/'].adapter.rooms === undefined) {
            console.log('rooms is undefined :\\','e');
        }

        var r = io.nsps['/'].adapter.rooms[room];

        if (typeof r === 'object') {
            return Object.keys(r);
        }
        else {
            return [];
        }
    }

    // Send error message to client.
    function sendError(number, msg, socket, room) {
        try {
            if (room !== undefined) {
                socket = socket.to(room);
            }
            socket.emit('errorMsg', {num: number, msg: msg});
        } catch(ex) {
            console.log(ex,'e');
        }
    }

    /* Connection Events */
    // On client connect (fires when anyone comes to site).
    io.on('connection', function(socket) {
        // Create an entry for new client in clients object.
        if (debug) { console.log('CLIENT, new:', socket.id); }

        // Initialize new client.
        var socketId = socket.id;
        clients[socketId] = null;

        // Deal with error messages.
        socket.on('error', function(data) {
            console.log('onError');
            console.log(data);
        });

        // Put new client in a room.
        socket.on('join', function(data, ack) {
            if (debug) { console.log('CLIENT, join:', socketId); }
            var room;
            var arena;
            var i;

            // Cycle through current rooms looking for an opening.
            for(i = 0; i < games.length; i++) {
                if(socketsInRoom(games[i]).length < maxPlayers) {
                    room = games[i];
                    arena = arenas[i];
                    if (debug) { console.log('ROOM, available:', games[i]); }
                    break;
                }
            }
            // If no rooms with less than maxPlayers, then make a new room.
            if(i === games.length) {
                room = makeGameId();
                arena = Math.floor(Math.random() * 3) + 1;
                arenas.push(arena);
                startTimer(i);
                if (debug) { console.log('ROOM, createing new:', room); }
            }

            // New client joins the room.
            socket.join(room, function (err) {
                if (!err) {
                    // Update client object with room info.
                    clients[socketId] = room;
                    clientsData[socketId] = data;

                    // Number of players in this room already.
                    var clientSockets = socketsInRoom(room);

                    // Tell other clients about the new client.
                    io.to(room).emit('playerJoined', {socketId: socketId, shipstuffs: clientsData[socketId]});
                    io.to(room).emit('newClientJoin', {socketId: socketId, shipstuffs: clientsData[socketId]});

                    var startPos = Math.floor(Math.random() * 39);

                    // Send list and ship object of cureent client socket id's back to new client.
                    ack({clientSockets: clientSockets, clientsData: clientsData, arena: arena, startPos: startPos});

                    if (debug) {
                        console.log('CLIENT ' + socketId + ' put in ROOM ' + room +
                            ' (' + clientSockets.length + '/' + maxPlayers + ')\n');
                    }

                // In case of error, send message to client.
                } else {
                    console.log(err);
                    sendError(3, 'client: can\'t join room', socket);
                }
            });
        });

        // When a player is ready.
        socket.on('signalReady', function(socketId) {
            // Room of player.
            var room = clients[socketId];

            // All players in room.
            var players = socketsInRoom(room);

            // If room is full, start countdown.
            if (players.length >= minPlayers) {
                if(debug) { console.log('ROOM: ' + room + ', ready.'); }

                socket.emit('roomStart', null, function() {
                    console.log('room start');
                });
            }
        });

        // Recieve data from each client.
        socket.on('gameUpdate', function(data) {
            // Update how long it's been since we heard from this client.
            var when = new Date();
            clientsTime[data.socketId] = when;

            // The `data` can be a player's ship or a player's shot. Ship data is sent from
            // `Ship.prototype.broadcast` in the `js/Ship.js` file from the client. Shot data comes
            // from a similar method/file.

            // Send recived data back to other clients in room.
            var room = clients[data.socketId];
            io.to(room).emit('clientUpdate', data);
        });

        // When I player leaves manually, update other's right away.
        socket.on('forfeit', function(data) {
            var room = clients[data.socketId];
            var players = socketsInRoom(room);
            if (debug) {
                console.log('CLIENT ' + data.socketId + ' taken out of ROOM ' + room +
                    ' (' + players.length + '/' + maxPlayers + ')\n');
            }
            io.to(room).emit('playerLeft', {socketId: data.socketId});
        });

        // Remove player from room.
        socket.on('disconnect', function() {
            if (debug) { console.log('CLIENT, disconnect:', socket.id); }
            var socketId = socket.id;

            // Get the effected room.
            var room = clients[socketId];

            // Remove them from clients object.
            clients[socketId] = null;
            delete clients[socketId];

            // Remove them from other clients objects.
            clientsData[socketId] = null;
            delete clientsData[socketId];
            clientsTime[socketId] = null;
            delete clientsTime[socketId];

            // Not dealing with clients not in a room.
            if(room !== null) {

                // Number of players left in that room.
                var players = socketsInRoom(room);

                if (debug) {
                    console.log('CLIENT ' + socketId + ' taken out of ROOM ' + room +
                        ' (' + players.length + '/' + maxPlayers + ')\n');
                }

                // Let other players know someone left.
                if(players.length > 0) {
                    io.to(room).emit('playerLeft', {socketId: socketId, playersCount: players.length});

                // Remove room if no one is left.
                } else {
                    var roomIndex = games.indexOf(room);
                    if (games.indexOf(room) !== -1) {
                        games.splice(roomIndex, 1);
                        arenas.splice(roomIndex, 1);
                        console.log('ROOM, destroyed:', room);
                    }
                }
            }
        });

        // Give a response to client, for timing latency.
        socket.on('ping', function() {
            socket.emit('pong');
        });

    });
}

module.exports = server;
