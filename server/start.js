/* global require */
var app = require('express')();
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var config = require('../config.json');

// CONFIGURE
mongoose.connect(config.database.connectionString); // connect to our database
require('./passport')(passport); // pass passport for configuration

// set up express application
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms

app.set('view engine', 'ejs'); // set up ejs for templating

// required for passport
app.use(session({ secret: 'donotcare' }));  // session secret
app.use(passport.initialize());
app.use(passport.session());                // persistent login sessions
app.use(flash());                           // use connect-flash for messages stored in session


// Routes
require('./routes.js')(app, passport);

var cport = config.port || 3000; // if not defined in config file, use 3000, server uses 8010.

// Start server.
var server = app.listen(cport, function() {
    console.log(server.address());
    var host = server.address().address;
    var port = server.address().port;
    console.log('HTTP Server running at http://' + host + ':' + port);
});

var io = require('socket.io')(server);
require('./gameserver.js')(io);
