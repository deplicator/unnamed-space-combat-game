/* global require, __dirname, module */

var path = require('path');
var app = require('express')();
var http = require('http').Server(app);
var mongodb = require('mongodb').MongoClient;
var assert = require('assert');
var config = require('../config.json');
var Player = require('./models/player.js');

// Check if there is player data in the session, if not return to title.
function isPlayerDataInSession(req, res, next) {
    if (typeof req.session.playerData !== 'undefined') {
        return next();
    }
    res.redirect('/title');
}

// Get ships out of database
var cursor;
var ships = [null];
mongodb.connect(config.database.connectionString, function(err, db) {
    assert.equal(null, err);
    cursor = db.collection('ships').find();
    cursor.each(function(err, doc) {
        assert.equal(err, null);
        if (doc !== null) {
            ships.push(doc);
        } else {
            db.close();
        }
    });
});

// Get weapons out of database
var weapons = [null];
mongodb.connect(config.database.connectionString, function(err, db) {
    assert.equal(null, err);
    cursor = db.collection('weapons').find();
    cursor.each(function(err, doc) {
        assert.equal(err, null);
        if (doc !== null) {
            weapons.push(doc);
        } else {
            db.close();
        }
    });
});

// Get actives out of database
var actives = [null];
mongodb.connect(config.database.connectionString, function(err, db) {
    assert.equal(null, err);
    cursor = db.collection('actives').find();
    cursor.each(function(err, doc) {
        assert.equal(err, null);
        if (doc !== null) {
            actives.push(doc);
        } else {
            db.close();
        }
    });
});

// Get passives out of database
var passives = [null];
mongodb.connect(config.database.connectionString, function(err, db) {
    assert.equal(null, err);
    cursor = db.collection('passives').find();
    cursor.each(function(err, doc) {
        assert.equal(err, null);
        if (doc !== null) {
            passives.push(doc);
        } else {
            db.close();
        }
    });
});

// Get powerups out of database
var powerups = [null];
mongodb.connect(config.database.connectionString, function(err, db) {
    assert.equal(null, err);
    cursor = db.collection('powerups').find();
    cursor.each(function(err, doc) {
        assert.equal(err, null);
        if (doc !== null) {
            powerups.push(doc);
        } else {
            db.close();
        }
    });
});

module.exports = function(app, passport) {
    var options = {root: path.join(__dirname, '..', 'client')};
    http.listen(8000);

    // Root and /title redirects to title screen.
    // http://stackoverflow.com/questions/15350025/express-js-single-routing-handler-for-multiple-routes-in-a-single-line
    app.get('/:var(|title)', function(req, res) {

        // If there is session data, go to ships page.
        // This is only used for players who joined with "Just Play" and refreshed the browser.
        if (typeof req.session.playerData !== 'undefined') {
            res.redirect('/ships');
        }

        res.render(path.join(__dirname, '..', 'client', 'title.ejs'), { message: req.flash('loginMessage') });
    });

    // Ship Select, the main screen to select a ship.
    app.get('/ships', function(req, res) {
        isPlayerDataInSession(req, res, function() {
            // registered and unregistered players should have playerData loaded at this point.
            res.render(path.join(__dirname, '..', 'client', 'ships.ejs'), { playerData: req.session.playerData, dbShips: ships });
        });
    });

    //About page
    app.get('/about', function(req, res){
      isPlayerDataInSession(req, res, function(){
        res.render(path.join(__dirname, '..', 'client', 'about.ejs'), {
          playerData: req.session.playerData
        });
      });
    });

    // Workshop
    app.get('/workshop', function(req, res) {
        isPlayerDataInSession(req, res, function() {
            // registered and unregistered players should have playerData loaded at this point.
            res.render(path.join(__dirname, '..', 'client', 'workshop.ejs'), {
                playerData: req.session.playerData,
                dbShips: ships,
                dbWeapons: weapons,
                dbActives: actives,
                dbPassives: passives
            });
        });
    });

    // Store
    app.get('/store', function(req, res) {
        isPlayerDataInSession(req, res, function() {
            // registered and unregistered players should have playerData loaded at this point.
            res.render(path.join(__dirname, '..', 'client', 'store.ejs'), {
                playerData: req.session.playerData,
                dbShips: ships,
                dbWeapons: weapons,
                dbActives: actives,
                dbPassives: passives
            });
        });
    });

    // Stats
    app.get('/stats', function(req, res) {
        isPlayerDataInSession(req, res, function() {
            // registered and unregistered players should have playerData loaded at this point.
            res.render(path.join(__dirname, '..', 'client', 'stats.ejs'), { playerData: req.session.playerData });
        });
    });

    // Settings page does not require user to be logged in, but changes will be lost.
    app.get('/profile', function(req, res) {
        isPlayerDataInSession(req, res, function() {
            // registered and unregistered players should have playerData loaded at this point.
            res.render(path.join(__dirname, '..', 'client', 'profile.ejs'), { playerData: req.session.playerData });
        });
    });

    // Settings page does not require user to be logged in, but changes will be lost.
    app.get('/settings', function(req, res) {
        isPlayerDataInSession(req, res, function() {
            // registered and unregistered players should have playerData loaded at this point.
            res.render(path.join(__dirname, '..', 'client', 'settings.ejs'), { playerData: req.session.playerData });
        });
    });

    // The game is played here.
    app.get('/game', function(req, res) {
        isPlayerDataInSession(req, res, function() {
            // Game needs player object (from session data) for selected ship object and stats,
            // and ships object (from database) for sprite paths.
            res.render(path.join(__dirname, '..', 'client', 'game.ejs'), {
                playerData: req.session.playerData,
                dbShips: ships,
                dbWeapons: weapons,
                dbActives: actives,
                dbPassives: passives,
                dbPowerups: powerups
            });
        });
    });

    // A client side only game environment to fly a ship around in.
    app.get('/practice', function(req, res) {
        isPlayerDataInSession(req, res, function() {
            res.render(path.join(__dirname, '..', 'client', 'practice.ejs'), { playerData: req.session.playerData, dbShips: ships });
        });
    });

    // Logout
    app.get('/logout', function(req, res) {
        // Logout only deletes passport session data, we delete ours this way.
        delete req.session.playerData;
        req.logout();
        res.redirect('/title');
    });

    // Create default session data for unregistered players.
    app.get('/justplay', function(req, res) {
        req.session.playerData = new Player();
        res.redirect('/ships');
    });

    // Get playerData for registered players.
    app.get('/registereduser', function(req, res) {
        req.session.playerData = req.user;//get from db;
        res.redirect('/ships');
    });

    // Facebook authentication.
    app.get('/auth/facebook',
        passport.authenticate('facebook', { scope : 'email' })
        // I think saving a current "Just Play" session before a user chooses to link their facebook
        // account will be handled here.
        // http://passportjs.org/docs/authenticate
    );

    // Handle the callback after facebook has authenticated the user.
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            successRedirect : '/registereduser',
            failureRedirect : '/title'
        })
    );

    // Sends any other file as is.
    app.get( '/*' , function(req, res) {
        //This is the current file they have requested
        var file = req.params[0];

        //Send the requesting client the file.
        res.sendFile(file, options);
    });

    // Saves player object to database and updates session data.
    app.post('/savePlayerObj', function(req, res) {

        // New player opbject passed from client.
        var updatedPlayerObj = req.body;

        // Empty array's passed in POST create undefined keys.
        var possibleEmpty = ['ships', 'weapons', 'active', 'passive'];
        for (var i = 0; i < possibleEmpty.length; i++) {
            if (updatedPlayerObj.inventory[possibleEmpty[i]] === undefined) {
                updatedPlayerObj.inventory[possibleEmpty[i]] = [];
            }
        }

        // Uses Mongoose to update player in db
        try {
            Player.findOneAndUpdate(
                {_id: req.session.playerData._id},  // Find this player by mongodb id.
                updatedPlayerObj,                   // Update everything.

                // For some reason we need this.
                function(err, player) {             // jshint ignore:line
                    //console.log(player);
                }
            );
        } catch(err) {
            console.log(err);
        }

        // Update session.
        req.session.playerData = updatedPlayerObj;

        // Must send back a response text.
        res.send('POST sent.');
    });

};
