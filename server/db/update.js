/* global require */
// This file updates static database items. It should be run from `gulp updatedb`.

var mongodb = require('mongodb').MongoClient;
var assert = require('assert');
var fs = require('fs');
var path = require('path');
var config = require('../../config.json');

var activedone = false;
var passivedone = false;
var powerupsdone = false;
var shipsdone = false;
var weaponsdone = false;

// Make sure all updates are done before closing the db connection.
function closeDB(db) {
    if(activedone && passivedone && powerupsdone && shipsdone && weaponsdone) {
        db.close();
    }
}

// Catch invalid json files. http://stackoverflow.com/a/29798770/2219970
function safelyParseJSON(json) {
    var parsed;

    try {
        parsed = JSON.parse(json);
    } catch (e) {
        console.log(e);
    }

    return parsed; // Could be undefined!
}

// Make an array of all the objects found in a particular folder.
function createObjects(p, cb) {

    // A new array with all the object inside.
    var alltehobjs = [];

    // Set some date info.
    var now = new Date();
    var jsonDate = now.toJSON();

    // Make a list of the files in this directory.
    var files = fs.readdirSync(p);

    // Look at each one.
    files.forEach(function(element) {
        // We're only interested in file with the right extention.
        if(element.substring(element.lastIndexOf('.'), element.length) === '.json') {

            // Make sure it's JSON
            var contents = fs.readFileSync(path.join(p, element)).toString();
            contents = safelyParseJSON(contents);

            // If it is JSON let's use it.
            if(contents !== undefined) {
                // If it's an object, just add it to the new array.
                if(contents.length === undefined) {
                    contents['lastupdate'] = jsonDate;
                    alltehobjs.push(contents);

                // If it's an array, loop through it and add each object.
                } else {
                    for(var i = 0; i < contents.length; i++) {
                        contents[i]['lastupdate'] = jsonDate;
                        alltehobjs.push(contents[i]);
                    }
                }

            // Let's tell people which files suck.
            } else {
                console.log('This file is not valid json:', path.join(p, element));
            }
        }
    });

    // Remove duplicates based on id. http://stackoverflow.com/a/2219024/2219970
    var arr = {};
    for (var i = 0, len = alltehobjs.length; i < len; i++) {
        arr[alltehobjs[i]['id']] = alltehobjs[i];
    }
    alltehobjs = [];
    for (var key in arr) {
        if(arr.hasOwnProperty(key)) {
            alltehobjs.push(arr[key]);
        }
    }

    // Now go do stuff with it.
    cb(alltehobjs);
}

// Make db connection and run updates.
mongodb.connect(config.database.connectionString, function(err, db) {
    assert.equal(null, err);
    console.log('\n    Connected to database.');

    // Update active modules.
    createObjects('./server/db/actives/', function (data) {
        db.collection('actives').remove({});
        if(data.length > 0) {
            db.collection('actives').insert(data, function(err) {
                assert.equal(err, null);
                activedone = true;
                console.log('    Updated active modules.');
                closeDB(db);
            });
        } else {
            console.log('    No active modules found in directory.');
            activedone = true;
        }
    });

    // Update passive modules.
    createObjects('./server/db/passives/', function (data) {
        db.collection('passives').remove({});
        if(data.length > 0) {
        db.collection('passives').insert(data, function(err) {
            assert.equal(err, null);
            passivedone = true;
            console.log('    Updated passive modules.');
            closeDB(db);
        });
    } else {
        console.log('    No passive modules found in directory.');
        passivedone = true;
    }
    });

    // Update powerups.
    createObjects('./server/db/powerups/', function (data) {
        db.collection('powerups').remove({});
        if(data.length > 0) {
            db.collection('powerups').insert(data, function(err) {
                assert.equal(err, null);
                powerupsdone = true;
                console.log('    Updated powerups.');
                closeDB(db);
            });
        } else {
            console.log('    No powerups found in directory.');
            powerupsdone = true;
        }
    });

    // Update ships.
    createObjects('./server/db/ships/', function (data) {
        db.collection('ships').remove({});
        if(data.length > 0) {
            db.collection('ships').insert(data, function(err) {
                assert.equal(err, null);
                shipsdone = true;
                console.log('    Updated ships.');
                closeDB(db);
            });
        } else {
            console.log('    No ships found in directory.');
            shipsdone = true;
        }
    });

    // Update weapons.
    createObjects('./server/db/weapons/', function (data) {
        db.collection('weapons').remove({});
        if(data.length > 0) {
            db.collection('weapons').insert(data, function(err) {
                assert.equal(err, null);
                weaponsdone = true;
                console.log('    Updated weapon modules.');
                closeDB(db);
            });
        } else {
            console.log('    No weapon modules found in directory.');
            weaponsdone = true;
        }
    });

});
