/* global require, process, module */
// Passport can be used to authetnticate with third party authenticators.

var FacebookStrategy = require('passport-facebook').Strategy;
var Player = require('./models/player.js');
var config = require('../config.json');

module.exports = function(passport) {

    // required for persistent login sessions
    passport.serializeUser(function(player, done) {
        done(null, player.id);
    });

    // used to deserialize the player
    passport.deserializeUser(function(id, done) {
        Player.findById(id, function(err, player) {
            done(err, player);
        });
    });

    // Facebook application authentication details.
    passport.use(new FacebookStrategy({
        clientID: config.authorization.facebook.AppID,
        clientSecret: config.authorization.facebook.AppSecret,
        callbackURL: config.authorization.facebook.callbackURL
    },

    // Facebook will send back the token and profile.
    function(token, refreshToken, profile, done) {
        process.nextTick(function() {

            // Search for player by Facebook id.
            Player.findOne({'profile.facebook.id': profile.id}, function(err, player) {

                // On error, stop everything and return it.
                if (err) { return done(err); }

                // If a player is found log them in.
                if (player) {
                    return done(null, player);

                // If no player found with that facebook id, create a new one.
                } else {
                    var newPlayer = new Player();

                    // Add facebook info to new player.
                    newPlayer.profile.facebook.id = profile.id;
                    newPlayer.profile.facebook.name  = profile.displayName;
                    newPlayer.profile.facebook.gender  = profile.gender;
                    newPlayer.profile.facebook.token = token;

                    // Save player to database.
                    newPlayer.save(function(err) {
                        if (err) { throw err; }

                        // On success, return new player.
                        return done(null, newPlayer);
                    });
                }

            });
        });

    }));

};
