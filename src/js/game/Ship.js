/*global
    PlayerObj, dbWeapons, dbPassives, dbActives, Shot
*/

/**
 * Represents a player's ship.
 * @class
 * @param {object} game - The game object this ship belongs to.
 * @param {string} socketId - Unique id of the player defined by socket.io.
 * @param {object} selectedShip - JSON of player's ship layout selected when they entered the game.
 */
function Ship(game, socketId, selectedShip, startPos) {
    'use strict';
    var self = this;
    // Set up some vairbales.
    this.me = false; // Is this me?
    if(socketId === socket.id) {
        this.me = true;
    }
    this.socketId = socketId;
    this.game = game;
    this.playerShipData = selectedShip;
    this.shipLevel = parseInt(this.playerShipData.level, 10);

    // Initialize broadcast data.
    this.broadcastData = {};
    this.broadcastData['alive'] = true;

    // Initialize round statistics.
    this.roundStats = {};  // used to get stats, lost if player forfeits.
    this.roundStats.credits = 100;
    this.roundStats.shotsFired = 0;
    this.roundStats.shotsHit = 0;
    this.roundStats.damageAbsorbed = 0;
    this.roundStats.damageDone = 0;
    this.roundStats.hitsTakenArmor = 0;
    this.roundStats.hitsTakenShield = 0;
    this.roundStats.shieldRegenTics = 0;
    this.roundStats.gamerTag = PlayerObj.profile.gamerTag;

    // Chose a random spot in the arena.
    if (startPos !== undefined) {
        this.startPos = startPos;
    } else {
        this.startPos = {x: 100, y: 100};
    }
    var startX = this.startPos.x;
    var startY = this.startPos.y;
    var startRot =  game.rnd.integerInRange(0, 359);

    // Setup player ship sprite.
    this.player = this.game.add.sprite(startX, startY, this.playerShipData.sprites.ship.name);
    this.player.name = this.playerShipData.name;
    this.player.socketId = socketId;

    // Enable sprite physics and physics characteristics.
    this.game.physics.p2.enable(this.player);
    this.player.body.isPlayer = true;
    this.player.body.socketId = socketId;
    this.player.body.angle = startRot;
    this.player.body.data.mass = 1; // Add mass to ships in the future.
    this.player.body.clearShapes();
    this.player.body.loadPolygon('greenShipShapes', this.playerShipData.sprites.ship.shape);
    this.player.body.setCollisionGroup(this.game.collisionGroups.shipsCG);
    this.player.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.wallsCG]);

    // Player's in game attributes.
    this.setupAttributes();

    // Setup particle emitters for thruster animations.
    this.setupThrusters();

    // An array to store damager ticker numbers.
    this.dmgTick = [];

    // part of setup not needed for opponets.
    if (this.me) {

        // Check for all collisions (specifically with this ship even though this function can
        // return any collision)
        this.game.physics.p2.setPostBroadphaseCallback(this.collision, this);

        this.healthCenter = this.game.add.sprite(32, 32, 'healthCenter');
        this.armorBar = this.game.add.sprite(2, 23, 'armorBar');
        this.shieldBar = this.game.add.sprite(145, 23, 'shieldBar');
        this.healthCenter.addChild(this.armorBar);
        this.healthCenter.addChild(this.shieldBar);
        this.healthCenter.fixedToCamera = true;
        // For ships that start with no shield.
        if (this.shieldMax === 0) {
            this.shieldBar.scale.setTo(0, 1);
        }

        // Make some bullets (based on what weapons I have)
        this.bullets = [];
        this.fireRate = [];
        this.nextFire = [];

        // Add weapons and modifiers.
        for (var i = 0; i < this.weapons.length; i++) {
            this.bullets[i] = this.game.add.physicsGroup(Phaser.Physics.P2JS);
            this.bullets[i].createMultiple(this.weapons[i].damage.maxShots, 'shot' + this.weapons[i].id);
            this.fireRate[i] = this.weapons[i].damage.fireRate;
            this.nextFire[i] = 0;
            if (this.playerShipData.bonus !== undefined) {
                for (var j = 0; j < this.playerShipData.bonus.length; j++) {
                    if (this.playerShipData.bonus[j].damageType === this.weapons[i].damage.type) {
                        this.weapons[i].damage.amount = this.weapons[i].damage.amount * this.playerShipData.bonus[j].modifier;
                    }
                    if (this.playerShipData.bonus[j].bonusType === 'range') {
                        this.weapons[i].damage.range = this.weapons[i].damage.range * this.playerShipData.bonus[j].modifier;
                    }
                }
            }
        }

        // Add passive module functions to ship.
        for (var i = 0; i < this.passives.length; i++) {
            var attributeEffected = Object.keys(this.passives[i].effect);
            attributeEffected.forEach(function(attribute) {
                // Is this a movement based passive?
                if(self.passives[i].type === 'movement') {
                    self.movement[attribute] *= self.passives[i].effect[attribute];

                // Is a standard attribute.
                } else {
                    self[attribute] *= self.passives[i].effect[attribute];
                }
            });
        }

        // Add active module function to ship.
        this.activeNextUse = 0;
        for (var i = 0; i < this.actives.length; i++) {
            // no longer needed now they're all functions
        }

        // Camera on me
        this.game.camera.follow(this.player);
        this.game.camera.deadzone = new Phaser.Rectangle(395, 245, 10, 10);
        this.game.camera.focusOnXY(startX, startY);


        // Keyboard input
        this.controls = {
            forward: this.game.input.keyboard.addKey(Phaser.Keyboard[PlayerObj.settings.controls.forward]),
            reverse: this.game.input.keyboard.addKey(Phaser.Keyboard[PlayerObj.settings.controls.reverse]),
            left: this.game.input.keyboard.addKey(Phaser.Keyboard[PlayerObj.settings.controls.left]),
            right: this.game.input.keyboard.addKey(Phaser.Keyboard[PlayerObj.settings.controls.right]),
            sleft: this.game.input.keyboard.addKey(Phaser.Keyboard[PlayerObj.settings.controls.sleft]),
            sright: this.game.input.keyboard.addKey(Phaser.Keyboard[PlayerObj.settings.controls.sright]),
            allStop: this.game.input.keyboard.addKey(Phaser.Keyboard[PlayerObj.settings.controls.allStop]),
            fire: this.game.input.keyboard.addKey(Phaser.Keyboard[PlayerObj.settings.controls.fire]),
            useActive: this.game.input.keyboard.addKey(Phaser.Keyboard[PlayerObj.settings.controls.useActive])
        };

        // Gamepad input
        this.game.input.gamepad.start();
        this.gamepad = this.game.input.gamepad.pad1;
    }
}

/**
 *  Initial attributes.
 */
Ship.prototype.setupAttributes = function() {
    this.movement = {};
    this.movement.forward = this.playerShipData.attributes.acceleration * 10;
    this.movement.reverse = this.playerShipData.attributes.acceleration * 2;
    this.movement.sidethrusters = this.playerShipData.attributes.acceleration * 2;
    this.movement.rotate = this.playerShipData.attributes.thrust * 5;
    // Max armor = (attribute * 10) + (attribute * ship level * 10)
    this.armorMax = (parseInt(this.playerShipData.attributes.armor, 10) * 50) +
                    (this.shipLevel * parseInt(this.playerShipData.attributes.armor, 10) * 10);
    this.armor = this.armorMax;
    this.armorRegen = parseInt(this.playerShipData.attributes.armorRegen, 10);

    // Max shield = (attribute * 25) + (attribute * ship level * 5)
    this.shieldMax = (parseInt(this.playerShipData.attributes.shield, 10) * 25) +
                     (this.shipLevel * parseInt(this.playerShipData.attributes.shield, 10) * 5);
    this.shield = this.shieldMax;
    this.shieldRegen = parseInt(this.playerShipData.attributes.shieldRegen, 10);
    this.regenTick = 0;
    this.lastHit = 0;

    // Add module details to this ship from db object.
    this.weapons = [];
    this.actives = [];
    this.passives = [];
    for(var i = 0; i < this.playerShipData.slots.length; i++) {
        var j;
        switch(this.playerShipData.slots[i].moduleType) {
            case 'weapon':
                for(j = 1; j < dbWeapons.length; j++) {
                    if(parseInt(dbWeapons[j].id, 10) === parseInt(this.playerShipData.slots[i].moduleId, 10)) {
                        this.weapons.push(dbWeapons[j]);
                    }
                }
                break;

            case 'active':
                for(j = 1; j < dbActives.length; j++) {
                    if(parseInt(dbActives[j].id, 10) === parseInt(this.playerShipData.slots[i].moduleId, 10)) {
                        this.actives.push(dbActives[j]);
                    }
                }
                break;

            case 'passive':
                for(j = 1; j < dbPassives.length; j++) {
                    if(parseInt(dbPassives[j].id, 10) === parseInt(this.playerShipData.slots[i].moduleId, 10)) {
                        this.passives.push(dbPassives[j]);
                    }
                }
                break;
        }
    }
};

/**
 *  All the code that iniializes thrusters, moved here because it's a chunk.
 */
Ship.prototype.setupThrusters = function() {
    // Setup particle emiters for thruster animations.
    this.emitters = {};
    this.emitters.forward = [];
    this.emitters.reverse = [];
    this.emitters.portbow = [];
    this.emitters.portaft = [];
    this.emitters.starboardbow = [];
    this.emitters.starboardaft = [];
    this.emitters.destruction = null;

    // Number of particle emitters for each direction.
    this.emitters.fwdlen = this.playerShipData.thrusters.forward.length;
    this.emitters.revlen = this.playerShipData.thrusters.reverse.length;
    this.emitters.pblen = this.playerShipData.thrusters.portbow.length;
    this.emitters.palen = this.playerShipData.thrusters.portaft.length;
    this.emitters.sblen = this.playerShipData.thrusters.starboardbow.length;
    this.emitters.salen = this.playerShipData.thrusters.starboardaft.length;

    // Forward Acceleration particle emmiters
    for(var i = 0; i < this.emitters.fwdlen; i++) {
        this.emitters.forward[i] = this.game.add.emitter();
        this.emitters.forward[i].makeParticles(this.playerShipData.thrusters.forward[i].particle);
        this.player.addChild(this.emitters.forward[i]);
        this.emitters.forward[i].x = parseInt(this.playerShipData.thrusters.forward[i].x, 10);
        this.emitters.forward[i].y = parseInt(this.playerShipData.thrusters.forward[i].y, 10);
        this.emitters.forward[i].angle = parseInt(this.playerShipData.thrusters.forward[i].angle, 10);
        this.emitters.forward[i].width = parseInt(this.playerShipData.thrusters.forward[i].width, 10);
        this.emitters.forward[i].lifespan = parseInt(this.playerShipData.thrusters.forward[i].lifespan, 10);
        this.emitters.forward[i].minParticleSpeed.x = parseInt(this.playerShipData.thrusters.forward[i].minParticleSpeed.x, 10);
        this.emitters.forward[i].minParticleSpeed.y = parseInt(this.playerShipData.thrusters.forward[i].minParticleSpeed.y, 10);
        this.emitters.forward[i].maxParticleSpeed.x = parseInt(this.playerShipData.thrusters.forward[i].maxParticleSpeed.x, 10);
        this.emitters.forward[i].maxParticleSpeed.y = parseInt(this.playerShipData.thrusters.forward[i].maxParticleSpeed.y, 10);
        this.emitters.forward[i].minParticleScale = parseFloat(this.playerShipData.thrusters.forward[i].minParticleScale, 10);
        this.emitters.forward[i].maxParticleScale = parseFloat(this.playerShipData.thrusters.forward[i].maxParticleScale, 10);
        this.emitters.forward[i].minRotation = parseInt(this.playerShipData.thrusters.forward[i].minRotation, 10);
        this.emitters.forward[i].maxRotation = parseInt(this.playerShipData.thrusters.forward[i].maxRotation, 10);
        this.emitters.forward[i].minParticleAlpha = parseFloat(this.playerShipData.thrusters.forward[i].minParticleAlpha, 10);
        this.emitters.forward[i].maxParticleAlpha = parseFloat(this.playerShipData.thrusters.forward[i].maxParticleAlpha, 10);
    }

    // Reverse Thrust particle emmiters
    for(var i = 0; i < this.emitters.revlen; i++) {
        this.emitters.reverse[i] = this.game.add.emitter();
        this.emitters.reverse[i].makeParticles(this.playerShipData.thrusters.reverse[i].particle);
        this.player.addChild(this.emitters.reverse[i]);
        this.emitters.reverse[i].x = parseInt(this.playerShipData.thrusters.reverse[i].x, 10);
        this.emitters.reverse[i].y = parseInt(this.playerShipData.thrusters.reverse[i].y, 10);
        this.emitters.reverse[i].angle = parseInt(this.playerShipData.thrusters.reverse[i].angle, 10);
        this.emitters.reverse[i].width = parseInt(this.playerShipData.thrusters.reverse[i].width, 10);
        this.emitters.reverse[i].lifespan = parseInt(this.playerShipData.thrusters.reverse[i].lifespan, 10);
        this.emitters.reverse[i].minParticleSpeed.x = parseInt(this.playerShipData.thrusters.reverse[i].minParticleSpeed.x, 10);
        this.emitters.reverse[i].minParticleSpeed.y = parseInt(this.playerShipData.thrusters.reverse[i].minParticleSpeed.y, 10);
        this.emitters.reverse[i].maxParticleSpeed.x = parseInt(this.playerShipData.thrusters.reverse[i].maxParticleSpeed.x, 10);
        this.emitters.reverse[i].maxParticleSpeed.y = parseInt(this.playerShipData.thrusters.reverse[i].maxParticleSpeed.y, 10);
        this.emitters.reverse[i].minParticleScale = parseFloat(this.playerShipData.thrusters.reverse[i].minParticleScale, 10);
        this.emitters.reverse[i].maxParticleScale = parseFloat(this.playerShipData.thrusters.reverse[i].maxParticleScale, 10);
        this.emitters.reverse[i].minRotation = parseInt(this.playerShipData.thrusters.reverse[i].minRotation, 10);
        this.emitters.reverse[i].maxRotation = parseInt(this.playerShipData.thrusters.reverse[i].maxRotation, 10);
        this.emitters.reverse[i].minParticleAlpha = parseFloat(this.playerShipData.thrusters.reverse[i].minParticleAlpha, 10);
        this.emitters.reverse[i].maxParticleAlpha = parseFloat(this.playerShipData.thrusters.reverse[i].maxParticleAlpha, 10);
    }

    // Port Bow Thruster particle emmiters
    for(i = 0; i < this.emitters.pblen; i++) {
        this.emitters.portbow[i] = this.game.add.emitter();
        this.emitters.portbow[i].makeParticles(this.playerShipData.thrusters.portbow[i].particle);
        this.player.addChild(this.emitters.portbow[i]);
        this.emitters.portbow[i].x = parseInt(this.playerShipData.thrusters.portbow[i].x, 10);
        this.emitters.portbow[i].y = parseInt(this.playerShipData.thrusters.portbow[i].y, 10);
        this.emitters.portbow[i].angle = parseInt(this.playerShipData.thrusters.portbow[i].angle, 10);
        this.emitters.portbow[i].width = parseInt(this.playerShipData.thrusters.portbow[i].width, 10);
        this.emitters.portbow[i].lifespan = parseInt(this.playerShipData.thrusters.portbow[i].lifespan, 10);
        this.emitters.portbow[i].minParticleSpeed.x = parseInt(this.playerShipData.thrusters.portbow[i].minParticleSpeed.x, 10);
        this.emitters.portbow[i].minParticleSpeed.y = parseInt(this.playerShipData.thrusters.portbow[i].minParticleSpeed.y, 10);
        this.emitters.portbow[i].maxParticleSpeed.x = parseInt(this.playerShipData.thrusters.portbow[i].maxParticleSpeed.x, 10);
        this.emitters.portbow[i].maxParticleSpeed.y = parseInt(this.playerShipData.thrusters.portbow[i].maxParticleSpeed.y, 10);
        this.emitters.portbow[i].minParticleScale = parseFloat(this.playerShipData.thrusters.portbow[i].minParticleScale, 10);
        this.emitters.portbow[i].maxParticleScale = parseFloat(this.playerShipData.thrusters.portbow[i].maxParticleScale, 10);
        this.emitters.portbow[i].minRotation = parseInt(this.playerShipData.thrusters.portbow[i].minRotation, 10);
        this.emitters.portbow[i].maxRotation = parseInt(this.playerShipData.thrusters.portbow[i].maxRotation, 10);
        this.emitters.portbow[i].minParticleAlpha = parseFloat(this.playerShipData.thrusters.portbow[i].minParticleAlpha, 10);
        this.emitters.portbow[i].maxParticleAlpha = parseFloat(this.playerShipData.thrusters.portbow[i].maxParticleAlpha, 10);
    }

    // Port Aft Thruster particle emmiters
    for(i = 0; i < this.emitters.palen; i++) {
        this.emitters.portaft[i] = this.game.add.emitter();
        this.emitters.portaft[i].makeParticles(this.playerShipData.thrusters.portaft[i].particle);
        this.player.addChild(this.emitters.portaft[i]);
        this.emitters.portaft[i].x = parseInt(this.playerShipData.thrusters.portaft[i].x, 10);
        this.emitters.portaft[i].y = parseInt(this.playerShipData.thrusters.portaft[i].y, 10);
        this.emitters.portaft[i].angle = parseInt(this.playerShipData.thrusters.portaft[i].angle, 10);
        this.emitters.portaft[i].width = parseInt(this.playerShipData.thrusters.portaft[i].width, 10);
        this.emitters.portaft[i].lifespan = parseInt(this.playerShipData.thrusters.portaft[i].lifespan, 10);
        this.emitters.portaft[i].minParticleSpeed.x = parseInt(this.playerShipData.thrusters.portaft[i].minParticleSpeed.x, 10);
        this.emitters.portaft[i].minParticleSpeed.y = parseInt(this.playerShipData.thrusters.portaft[i].minParticleSpeed.y, 10);
        this.emitters.portaft[i].maxParticleSpeed.x = parseInt(this.playerShipData.thrusters.portaft[i].maxParticleSpeed.x, 10);
        this.emitters.portaft[i].maxParticleSpeed.y = parseInt(this.playerShipData.thrusters.portaft[i].maxParticleSpeed.y, 10);
        this.emitters.portaft[i].minParticleScale = parseFloat(this.playerShipData.thrusters.portaft[i].minParticleScale, 10);
        this.emitters.portaft[i].maxParticleScale = parseFloat(this.playerShipData.thrusters.portaft[i].maxParticleScale, 10);
        this.emitters.portaft[i].minRotation = parseInt(this.playerShipData.thrusters.portaft[i].minRotation, 10);
        this.emitters.portaft[i].maxRotation = parseInt(this.playerShipData.thrusters.portaft[i].maxRotation, 10);
        this.emitters.portaft[i].minParticleAlpha = parseFloat(this.playerShipData.thrusters.portaft[i].minParticleAlpha, 10);
        this.emitters.portaft[i].maxParticleAlpha = parseFloat(this.playerShipData.thrusters.portaft[i].maxParticleAlpha, 10);
    }

    // Starboard Bow Thruster particle emmiters
    for(i = 0; i < this.emitters.sblen; i++) {
        this.emitters.starboardbow[i] = this.game.add.emitter();
        this.emitters.starboardbow[i].makeParticles(this.playerShipData.thrusters.starboardbow[i].particle);
        this.player.addChild(this.emitters.starboardbow[i]);
        this.emitters.starboardbow[i].x = parseInt(this.playerShipData.thrusters.starboardbow[i].x, 10);
        this.emitters.starboardbow[i].y = parseInt(this.playerShipData.thrusters.starboardbow[i].y, 10);
        this.emitters.starboardbow[i].angle = parseInt(this.playerShipData.thrusters.starboardbow[i].angle, 10);
        this.emitters.starboardbow[i].width = parseInt(this.playerShipData.thrusters.starboardbow[i].width, 10);
        this.emitters.starboardbow[i].lifespan = parseInt(this.playerShipData.thrusters.starboardbow[i].lifespan, 10);
        this.emitters.starboardbow[i].minParticleSpeed.x = parseInt(this.playerShipData.thrusters.starboardbow[i].minParticleSpeed.x, 10);
        this.emitters.starboardbow[i].minParticleSpeed.y = parseInt(this.playerShipData.thrusters.starboardbow[i].minParticleSpeed.y, 10);
        this.emitters.starboardbow[i].maxParticleSpeed.x = parseInt(this.playerShipData.thrusters.starboardbow[i].maxParticleSpeed.x, 10);
        this.emitters.starboardbow[i].maxParticleSpeed.y = parseInt(this.playerShipData.thrusters.starboardbow[i].maxParticleSpeed.y, 10);
        this.emitters.starboardbow[i].minParticleScale = parseFloat(this.playerShipData.thrusters.starboardbow[i].minParticleScale, 10);
        this.emitters.starboardbow[i].maxParticleScale = parseFloat(this.playerShipData.thrusters.starboardbow[i].maxParticleScale, 10);
        this.emitters.starboardbow[i].minRotation = parseInt(this.playerShipData.thrusters.starboardbow[i].minRotation, 10);
        this.emitters.starboardbow[i].maxRotation = parseInt(this.playerShipData.thrusters.starboardbow[i].maxRotation, 10);
        this.emitters.starboardbow[i].minParticleAlpha = parseFloat(this.playerShipData.thrusters.starboardbow[i].minParticleAlpha, 10);
        this.emitters.starboardbow[i].maxParticleAlpha = parseFloat(this.playerShipData.thrusters.starboardbow[i].maxParticleAlpha, 10);
    }

    // Starboard Aft Thruster particle emmiters
    for(i = 0; i < this.emitters.salen; i++) {
        this.emitters.starboardaft[i] = this.game.add.emitter();
        this.emitters.starboardaft[i].makeParticles(this.playerShipData.thrusters.starboardaft[i].particle);
        this.player.addChild(this.emitters.starboardaft[i]);
        this.emitters.starboardaft[i].x = parseInt(this.playerShipData.thrusters.starboardaft[i].x, 10);
        this.emitters.starboardaft[i].y = parseInt(this.playerShipData.thrusters.starboardaft[i].y, 10);
        this.emitters.starboardaft[i].angle = parseInt(this.playerShipData.thrusters.starboardaft[i].angle, 10);
        this.emitters.starboardaft[i].width = parseInt(this.playerShipData.thrusters.starboardaft[i].width, 10);
        this.emitters.starboardaft[i].lifespan = parseInt(this.playerShipData.thrusters.starboardaft[i].lifespan, 10);
        this.emitters.starboardaft[i].minParticleSpeed.x = parseInt(this.playerShipData.thrusters.starboardaft[i].minParticleSpeed.x, 10);
        this.emitters.starboardaft[i].minParticleSpeed.y = parseInt(this.playerShipData.thrusters.starboardaft[i].minParticleSpeed.y, 10);
        this.emitters.starboardaft[i].maxParticleSpeed.x = parseInt(this.playerShipData.thrusters.starboardaft[i].maxParticleSpeed.x, 10);
        this.emitters.starboardaft[i].maxParticleSpeed.y = parseInt(this.playerShipData.thrusters.starboardaft[i].maxParticleSpeed.y, 10);
        this.emitters.starboardaft[i].minParticleScale = parseFloat(this.playerShipData.thrusters.starboardaft[i].minParticleScale, 10);
        this.emitters.starboardaft[i].maxParticleScale = parseFloat(this.playerShipData.thrusters.starboardaft[i].maxParticleScale, 10);
        this.emitters.starboardaft[i].minRotation = parseInt(this.playerShipData.thrusters.starboardaft[i].minRotation, 10);
        this.emitters.starboardaft[i].maxRotation = parseInt(this.playerShipData.thrusters.starboardaft[i].maxRotation, 10);
        this.emitters.starboardaft[i].minParticleAlpha = parseFloat(this.playerShipData.thrusters.starboardaft[i].minParticleAlpha, 10);
        this.emitters.starboardaft[i].maxParticleAlpha = parseFloat(this.playerShipData.thrusters.starboardaft[i].maxParticleAlpha, 10);
    }

    this.emitters.destruction = this.game.add.emitter();
    this.emitters.destruction.makeParticles('green-explosion');
    this.player.addChild(this.emitters.destruction);
    this.emitters.destruction.x = 0;
    this.emitters.destruction.y = 0;
    this.emitters.destruction.width = 40;
    this.emitters.destruction.lifespan = 200;
    this.emitters.destruction.minParticleSpeed.x = -100;
    this.emitters.destruction.minParticleSpeed.y = -100;
    this.emitters.destruction.maxParticleSpeed.x = 100;
    this.emitters.destruction.maxParticleSpeed.y = 100;
    this.emitters.destruction.minParticleScale = 0.5;
    this.emitters.destruction.maxParticleScale = 1;
    this.emitters.destruction.minRotation = -360;
    this.emitters.destruction.maxRotation = 360;
    this.emitters.destruction.minParticleAlpha = 0.75;
    this.emitters.destruction.maxParticleAlpha = 1;
};

/**
 * Called on game update loop.
 * @param {object} [data] - Broadcast data from other ships.
 */
Ship.prototype.update = function(data) {
    if(this.me) {

        this.regenUpdate();

        this.game.world.wrap(this.player.body);

        // forward
        if (this.controls.forward.isDown || this.gamepad.isDown(Phaser.Gamepad.XBOX360_A) ||
        this.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_UP) ||
        this.gamepad.axis(Phaser.Gamepad.XBOX360_STICK_RIGHT_Y) < -0.1) {
            if (this.gamepad.axis(Phaser.Gamepad.XBOX360_STICK_RIGHT_Y) < -0.1) {
                this.player.body.thrust(this.movement.forward * this.gamepad.axis(Phaser.Gamepad.XBOX360_STICK_RIGHT_Y) * -1);
            } else {
                this.player.body.thrust(this.movement.forward);
            }
            this.broadcastData['playerAccelerating'] = true;
            for(var i = 0; i < this.emitters.fwdlen; i++) {
                this.emitters.forward[i].emitParticle();
            }
        } else {
            this.broadcastData['playerAccelerating'] = false;
        }

        // reverse
        if (this.controls.reverse.isDown || this.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_DOWN) ||
        this.gamepad.axis(Phaser.Gamepad.XBOX360_STICK_RIGHT_Y) > 0.1) {
            this.player.body.reverse(this.movement.reverse);
            for(var i = 0; i < this.emitters.revlen; i++) {
                this.emitters.reverse[i].emitParticle();
            }
        }

        // strafe left, right
        if (this.controls.sleft.isDown ||
        this.gamepad.axis(Phaser.Gamepad.XBOX360_STICK_RIGHT_X) > 0.1) {
            this.player.body.thrustLeft(this.movement.sidethrusters);
            for(var i = 0; i < this.emitters.sblen; i++) {
                this.emitters.starboardbow[i].emitParticle();
            }
            for(var i = 0; i < this.emitters.salen; i++) {
                this.emitters.starboardaft[i].emitParticle();
            }

        } else if (this.controls.sright.isDown ||
        this.gamepad.axis(Phaser.Gamepad.XBOX360_STICK_RIGHT_X) < -0.1) {
            this.player.body.thrustRight(this.movement.sidethrusters);
            for(var i = 0; i < this.emitters.pblen; i++) {
                this.emitters.portbow[i].emitParticle();
            }
            for(var i = 0; i < this.emitters.palen; i++) {
                this.emitters.portaft[i].emitParticle();
            }
        }

        // rotate left, right.
        if (this.controls.left.isDown || this.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_LEFT) ||
        this.gamepad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) < -0.1) {
            this.player.body.rotateLeft(this.movement.rotate);
            for(var i = 0; i < this.emitters.palen; i++) {
                this.emitters.portaft[i].emitParticle();
            }
            for(var i = 0; i < this.emitters.sblen; i++) {
                this.emitters.starboardbow[i].emitParticle();
            }

        } else if (this.controls.right.isDown || this.gamepad.isDown(Phaser.Gamepad.XBOX360_DPAD_RIGHT) ||
        this.gamepad.axis(Phaser.Gamepad.XBOX360_STICK_LEFT_X) > 0.1) {
            this.player.body.rotateRight(this.movement.rotate);
            for(var i = 0; i < this.emitters.pblen; i++) {
                this.emitters.portbow[i].emitParticle();
            }
            for(var i = 0; i < this.emitters.salen; i++) {
                this.emitters.starboardaft[i].emitParticle();
            }
        } else {
            this.player.body.setZeroRotation();
        }

        if (this.controls.allStop.isDown) {
            // figure out which way I'm going and what thruster to fire to come to a full stop.
        }

        // Fire weapons.
        if (this.controls.fire.isDown || this.gamepad.justPressed(Phaser.Gamepad.XBOX360_X) ||
        this.gamepad.isDown(Phaser.Gamepad.XBOX360_RIGHT_TRIGGER)) {
            this.fire();
        }

        // Use active module.
        this.controls.useActive.onDown.add(this.useActive, this);
        if(this.gamepad.justPressed(Phaser.Gamepad.XBOX360_Y) ||
        this.gamepad.justPressed(Phaser.Gamepad.XBOX360_RIGHT_BUMPER)) {
            this.useActive();
        }

    // If not me, it's an update from another ship.
    } else {
        if (this.player.body !== null) {

            this.player.body.data.force = data['playerForce'];

            // Get position from server (interpolation causes problems).
            this.player.body.data.position = data.playerPos;

            // Interpolate current angle, and veolcity with data from server.
            this.player.body.data.angle = (this.player.body.data.angle + data.playerAngle) / 2;
            this.player.body.data.velocity[0] = (this.player.body.data.velocity[0] + data.playerVel[0]) / 2;
            this.player.body.data.velocity[1] = (this.player.body.data.velocity[1] + data.playerVel[1]) / 2;

            if(data['playerAccelerating']) {
                for(var i = 0; i < this.emitters.fwdlen; i++) {
                    this.emitters.forward[i].emitParticle();
                }
            }
            if (!data['alive']) {
                this.destruction();
            }
        }
    }
};

/**
 * Figure out what hit what and what to do about it.
 * @param {object} a - First object involved in collision.
 * @param {object} b - Second object involved in collison.
 */
Ship.prototype.collision = function(a, b) {
    // this method deals with all collisions (litereally everything not just this ship).
    // this first if statement filters out collisions between yourself and your shots (because they
    // start at the same place it's a detected collison).

    // All about shots.
    if (a.sprite.socketId !== b.sprite.socketId) {

        // If both are bullets, ignore collision.
        if (a.sprite.isBullet && b.sprite.isBullet) {
            return false;
        }

        var dmg;
        var dmgType;

        // If either object is a bullet, handle it, and if it's me update my health stats
        if (a.sprite.isBullet) {
            dmg = a.sprite.dmg;
            dmgType = a.sprite.dmgType;
            a.sprite.kill();
            if(b.sprite.socketId === this.socketId) {
                this.takeHit(dmg, dmgType, a.sprite.socketId);
            }
        }

        if (b.sprite.isBullet) {
            dmg = b.sprite.dmg;
            dmgType = b.sprite.dmgType;
            b.sprite.kill();
            if(a.sprite.socketId === this.socketId) {
                this.takeHit(dmg, dmgType, b.sprite.socketId);
            }
        }
    }

    // All about powerups.
    if (a.sprite.isPowerup || b.sprite.isPowerup) {

        // Figure out which one is the powerup.
        var powerup = (a.sprite.isPowerup) ? a : b;

        // Only effected by player ships.
        if (!a.sprite.isBullet && !b.sprite.isBullet) {

            // Only who touched it gets the power up.
            if(a.sprite.socketId === this.socketId || b.sprite.socketId === this.socketId) {
                this.powerupEffect(powerup.sprite.effect);
            }

            // Kill for everyone.
            powerup.sprite.kill();
            return false;
        }

    }

    return true;
};

/**
 * Calculates damage done on a hit to this ship, broadcasts that data back to shot owner.
 * @param {number} dmg - Damage passed from shot data.
 * @param {string} dmgType - Damage type passed from shot data.
 * @param {string} fromWho - Socket of shot owner.
 */
Ship.prototype.takeHit = function(dmg, dmgType, fromWho) {
    var self = this;

    this.lastHit = this.game.time.now;

    var augmentedDmg;

    // Damage shield before armor.
    if(this.shield > 0) {
        switch(dmgType) {
            case 'kinetic': // kinetic damage is less effective against shield.
                augmentedDmg = dmg * 0.75;
                break;

            case 'energy':
                augmentedDmg = dmg;
                break;

            case 'combo': // combo damage is less effective against everything.
                augmentedDmg = dmg * 0.9;
                break;
        }
        self.shield = self.shield - augmentedDmg;
        self.roundStats.hitsTakenShield++;
        if (self.shield < 0) {
            self.shield = 0;
        }

        // Show damage pop up text.
        self.dmgText = self.game.add.text(self.player.x, self.player.y, Math.round(augmentedDmg), {font: '30px Arial', fill: '#162CD8', align: 'center'});
        self.dmgTick.push(self.dmgText);

        // Remove damage pop up text after a short time.
        if (self.dmgTick.length === 1) {
            self.dmgRemover = setInterval(function() {
                if (self.dmgTick[0] !== undefined) {
                    self.dmgTick[0].destroy();
                    self.dmgTick.shift();
                } else {
                    clearInterval(self.dmgRemover);
                }
            }, 250);
        }

    // No shield, damage armor.
    } else {
        switch(dmgType) {
            case 'kinetic':
                augmentedDmg = dmg;
                break;

            case 'energy':  // energy damage is less effective against armor.
                augmentedDmg = dmg * 0.75;
                break;

            case 'combo': // combo damage is less effective against everything.
                augmentedDmg = dmg * 0.9;
                break;
        }
        self.armor = self.armor - dmg;
        self.roundStats.hitsTakenArmor++;

        // Show damage pop up text.
        self.dmgText = self.game.add.text(self.player.x, self.player.y, Math.round(augmentedDmg), {font: '30px Arial', fill: '#D81616', align: 'center'});
        self.dmgTick.push(self.dmgText);

        // Remove damage pop up text after a short time.
        if (self.dmgTick.length === 1) {
            self.dmgRemover = setInterval(function() {
                if (self.dmgTick[0] !== undefined) {
                    self.dmgTick[0].destroy();
                    self.dmgTick.shift();
                } else {
                    clearInterval(self.dmgRemover);
                }
            }, 250);
        }

    }

    this.roundStats.damageAbsorbed += augmentedDmg;

    var broadcastShotData = {};
    broadcastShotData['socketId'] = socket.id;
    broadcastShotData['whodidit'] = fromWho;
    broadcastShotData['ship'] = false;
    broadcastShotData['shotUpdate'] = true;
    broadcastShotData['damageDone'] = augmentedDmg;
    broadcastShotData['playerPos'] = {x: self.player.x, y:self.player.y};
    socket.emit('gameUpdate', broadcastShotData);

    if(this.armor <= 0) {
        self.destruction();
    }
    this.healthBars();
};

/**
 * Show numerical damage pop up over ship.
 * @param {number} damage - The amount of damage done.
 * @param {object} playerPos - Location the player was in when hit {x: #, y: #}.
 */
Ship.prototype.dmgTextPopup = function(damage, playerPos) {
    var self = this;

    // Show damage pop up text.
    self.dmgText = self.game.add.text(playerPos.x, playerPos.y, Math.round(damage), {font: '30px Arial', fill: '#D81616', align: 'center'});
    self.dmgTick.push(self.dmgText);

    // Remove damage pop up text after a short time.
    if (self.dmgTick.length === 1) {
        self.dmgRemover = setInterval(function() {
            if (self.dmgTick[0] !== undefined) {
                self.dmgTick[0].destroy();
                self.dmgTick.shift();
            } else {
                clearInterval(self.dmgRemover);
            }
        }, 250);
    }
};

/**
 * Regenerate shilds.
 */
Ship.prototype.regenUpdate = function() {

    if (this.shieldRegen > 0) {
        // shield only start to regenerate ...
        if (this.shield < this.shieldMax &&             // if shield are below max
            this.game.time.now > this.lastHit + 5000 && // if havsen't been hit in 5 seconds
            this.game.time.now > this.regenTick) {      // and only once per second

            // Regen tick is x shield points per second for all ships.
            this.regenTick = this.game.time.now + 1000;
            this.shield = this.shield + this.shieldRegen;
            this.healthBars();
        }
    }

    if (this.armorRegen > 0) {
        // Armor only start to regenerate ...
        if (this.armor < this.armorMax &&               // if armor is below max
            this.game.time.now > this.lastHit + 2500 && // if havsen't been hit in 2.5 seconds
            this.game.time.now > this.regenTick) {      // and only once per second

            // Regen tick is x armor points per second for all ships.
            this.regenTick = this.game.time.now + 1000;
            this.armor = this.armor + this.armorRegen;
            this.healthBars();
        }
    }
};

/**
 * Set and update health bars.
 */
Ship.prototype.healthBars = function() {
    //this.game.add.tween(this.shieldBar.scale).to({x: 0.5, y: 0.5});
    // Armor max or greater (powerups can make it greater).
    var armorPercent = this.armor / this.armorMax;
    if(armorPercent > 1) {
        this.armorBar.scale.setTo(1, 1);
    } else if (armorPercent < 0) {
        this.armorBar.scale.setTo(0, 1);
    } else {
        this.armorBar.scale.setTo(armorPercent, 1);
    }

    var shieldPercent = this.shield / this.shieldMax;
    if(shieldPercent > 1) {
        this.shieldBar.scale.setTo(1, 1);
    } else if (shieldPercent < 0) {
        this.shieldBar.scale.setTo(0, 1);
    } else {
        this.shieldBar.scale.setTo(shieldPercent, 1);
    }
};

/**
 * Shoot a shot.
 */
Ship.prototype.fire = function() {
    var shotx;
    if (this.weapons.length === 0) {
        shotx = this.player.x;
    } else {
        shotx = this.player.x + 15;
    }

    for (var i = 0; i < this.weapons.length; i++) {
        if(this.game.time.now > this.nextFire[i]) {

            // Pause until can fire again
            this.nextFire[i] = this.game.time.now + this.fireRate[i];

            // Pull a bullet from current max.
            this.bullet = this.bullets[i].getFirstExists(false);

            // If not at max shots on screen.
            if(this.bullet) {
                this.roundStats.shotsFired++;
                if(i === 1) { shotx = this.player.x - 15; }

                var ShotData = {
                    socketId: socket.id,
                    shot: true,
                    playerX: shotx,
                    playerY: this.player.y,
                    velX: this.player.body.data.velocity[0],
                    velY: this.player.body.data.velocity[1],
                    playerRot: this.player.rotation,
                    sprite: this.bullet.key,
                    weaponData: this.weapons[i].damage,
                    bulletVel: 500
                };

                // Pass info to shot class, this player's socket.id, the bullet, and this players weapon object
                var shot = new Shot(ShotData);

                // Broadcast to other players.
                shot.broadcast();
            }
        }
    }
};

/**
 * Use installed Active module.
 */
Ship.prototype.useActive = function() {
    var self = this;

    // Ships can only have a single active module for now.
    if (self.actives[0] !== undefined) {

        // Check cooldown timer.
        if (self.game.time.now > self.activeNextUse) {

            // Reset cooldown timer.
            this.activeNextUse = this.game.time.now + self.actives[0].cooldown;

            // Use the active module.
            self.activeModuleFunctions[self.actives[0].function](self);
        }
    }
};

/**
 * Defines all possible active modules a ship can have. Note this is an object and not a function.
 * these functions must be passed the Ship object that calls them (the context parameter).
 */
Ship.prototype.activeModuleFunctions = {

    // Active Module 101: Polar Swap
    a101: function (context) {
        // Reverse velocity.
        context.player.body.data.velocity[0] *= -1;
        context.player.body.data.velocity[1] *= -1;
    },

    // Active Module 102: Invertersioner
    a102: function (context) {
        // Flip's player ship on y axis (bow to aft).
        context.player.body.data.angle += 3.14159;
    },

    // Active Module 103: Portable Wormhole
    a103: function (context) {
        // Teleport forward a short distance.
        context.player.body.data.position[0] = context.player.body.data.position[0] + (Math.cos(context.player.body.data.angle + 1.570795) * 7);
        context.player.body.data.position[1] = context.player.body.data.position[1] + (Math.sin(context.player.body.data.angle + 1.570795) * 7);
    },

    // Active Module 104: Simple Mines
    a104: function (context) {

        var mineData = {
            socketId: context.player.socketId,
            shot: true,
            playerX: context.player.x,
            playerY: context.player.y,
            velX: context.player.body.data.velocity[0],
            velY: context.player.body.data.velocity[1],
            playerRot: context.player.rotation,
            sprite: 'a104-mine',
            weaponData: {
                'type': 'combo',
                'amount': 25,
                'fireRate': 500,
                'range': 5000,
                'maxShots': 5
            },
            bulletVel: 0,
            animation: true
        };

        var mine = new Shot(mineData);
        mine.broadcast();
    }
};

/**
 * Update ship with power up effect.
 * @param {string} effect - What the effect is.
 */
Ship.prototype.powerupEffect = function(effect) {
    var self = this;

    switch(effect) {
        case 'speed':
            var oldforward = self.movement.forward;
            var oldreverse = self.movement.reverse;
            var oldsidethrusters = self.movement.sidethrusters;
            var oldrotate = self.movement.rotate;
            self.movement.forward *= 2.5;
            self.movement.reverse *= 2.5;
            self.movement.sidethrusters *= 2.5;
            self.movement.rotate *= 2.5;

            $('#game-message').html('Speed Boost... Go! Go! Go! <span></span>');

            // Return to original speed after 10 seconds, now with countdown!
            var countDown = 10;
            var puDuration = setInterval(function () {
                $('#game-message span').html(countDown - 1);
                countDown--;
                if(countDown === 0) {
                    $('#game-message').html('');
                    self.movement.forward = oldforward;
                    self.movement.reverse = oldreverse;
                    self.movement.sidethrusters = oldsidethrusters;
                    self.movement.rotate = oldrotate;
                    clearInterval(puDuration);
                }
            }, 1100);
            break;

        case 'damage':
            // Loop through all weapons to increase damage.
            var olddmg = []
            for (var i = 0; i < self.weapons.length; i++) {
                olddmg[i] = self.weapons[i].damage.amount;
                self.weapons[i].damage.amount *= 2.5;
            }

            $('#game-message').html('Damage Boost! <span></span>');

            // Return to normal damage after 10 seconds.
            var countDown = 10;
            var puDuration = setInterval(function () {
                $('#game-message span').html(countDown - 1);
                countDown--;
                if(countDown === 0) {
                    for (var i = 0; i < self.weapons.length; i++) {
                        self.weapons[i].damage.amount = olddmg[i];
                    }
                    $('#game-message').html('');
                    clearInterval(puDuration);
                }
            }, 1100);
            break;

        case 'heal':
            // Give static boost to armor and shileds.
            var armorHeal = self.armorMax / 2;
            var shieldHeal = self.armorMax / 2;
            // Don't over heal armor.
            if(self.armor + armorHeal >= self.armorMax) {
                self.armor = self.armorMax;
            } else {
                self.armor += armorHeal;
            }
            // Don't over heal shields.
            if(self.shield + shieldHeal >= self.shieldMax) {
                self.shield = self.shieldMax;
            } else {
                self.shield += shieldHeal;
            }

            $('#game-message').html('Healz!!!!1');

            setTimeout(function () {
                $('#game-message').html('');
            }, 10000);
            self.healthBars();
            break;
    }
};

/**
 * Tell server what I'm doing.
 */
Ship.prototype.broadcast = function() {
    this.broadcastData['socketId'] = socket.id;
    this.broadcastData['ship'] = true; // Differentiate between ships and shots.
    this.broadcastData['playerPos'] = this.player.body.data.position;
    this.broadcastData['playerAngle'] = this.player.body.data.angle;
    this.broadcastData['playerVel'] = this.player.body.data.velocity;
    this.broadcastData['playerForce'] = this.player.body.data.force;
    socket.emit('gameUpdate', this.broadcastData);
};

/**
 * Tell people I died.
 */
Ship.prototype.destruction = function() {
    var self = this;
    this.broadcastData['alive'] = false;
    this.emitters.destruction.start(false, 500, 40, 10);
    //stats
    setTimeout(function() {
        if(self.me) {
            PlayerObj.stats.credit = parseInt(PlayerObj.stats.credit) + parseInt(self.roundStats.damageDone) + 100;
            PlayerObj.stats.damageAbsorbed = parseInt(PlayerObj.stats.damageAbsorbed) + parseInt(self.roundStats.damageAbsorbed);
            PlayerObj.stats.hitsTakenArmor = parseInt(PlayerObj.stats.hitsTakenArmor) + parseInt(self.roundStats.hitsTakenArmor);
            PlayerObj.stats.hitsTakenShield = parseInt(PlayerObj.stats.hitsTakenShield) + parseInt(self.roundStats.hitsTakenShield);
            PlayerObj.stats.shotsFired = parseInt(PlayerObj.stats.shotsFired) + parseInt(self.roundStats.shotsFired);
            PlayerObj.stats.shotsHit = parseInt(PlayerObj.stats.shotsHit) + parseInt(self.roundStats.shotsHit);

            //call DeathState after player dies and PlayerObj is saved
            var sFired=parseInt(self.roundStats.shotsFired);
            var sHit=parseInt(self.roundStats.shotsHit);
            this.game.state.start('death',true,false,sFired,sHit);
            console.log("11--"+self.roundStats.shotsFired+"--"+self.roundStats.shotsHit);

            $.ajax({
                method: 'POST',
                url: '/savePlayerObj',
                data: PlayerObj
            }).done(function() {
                //location.reload(); ///THIS!!!

            })
            //this.game.state.start('death', false, false, data);
            //confirm('Shots Fired: '+PlayerObj.stats.shotsFired+''+'\nShots Hit: '+PlayerObj.stats.shotsHit);
        }
        //setTimeout(myFunction, 3000);
        self.destroy();

    }, 1000);
};

/**
 * Remove sprite from game world when player leaves.
 */
Ship.prototype.destroy = function() {
    this.player.destroy();
};

/**
 *
 */
Ship.prototype.isAlive = function(data){
    return this.broadcastData['alive'];
};
