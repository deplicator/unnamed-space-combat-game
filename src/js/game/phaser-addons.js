/**
* Applies a force to the Body that causes it to 'thrust' to the left, based on its current angle and the given speed.
* The speed is represented in pixels per second. So a value of 100 would move 100 pixels in 1 second (1000ms).
*
* @method Phaser.Physics.P2.Body#thrustLeft
* @param {number} speed - The speed at which it should move to the left.
*/
Phaser.Physics.P2.Body.prototype.thrustLeft = function(speed) {
    var magnitude = this.world.pxmi(-speed);
    var angle = this.data.angle;
    this.data.force[0] += magnitude * Math.cos(angle);
    this.data.force[1] += magnitude * Math.sin(angle);
};

/**
* Applies a force to the Body that causes it to 'thrust' to the right, based on its current angle and the given speed.
* The speed is represented in pixels per second. So a value of 100 would move 100 pixels in 1 second (1000ms).
*
* @method Phaser.Physics.P2.Body#thrustRight
* @param {number} speed - The speed at which it should move to the right.
*/
Phaser.Physics.P2.Body.prototype.thrustRight = function(speed) {
    var magnitude = this.world.pxmi(-speed);
    var angle = this.data.angle;
    this.data.force[0] -= magnitude * Math.cos(angle);
    this.data.force[1] -= magnitude * Math.sin(angle);
};
