/*global
    game
*/

/**
 * Shot class (used for each bullet/missle/shot/laser anything fired from a ship).
 * @param {object} data - Information about the shot and player that fired it..
 */
function Shot(data) {
    this.data = data;

    // Make sprite
    this.bullet = game.add.sprite(data.playerX, data.playerY, data.sprite);


    // Set up identification
    this.bullet.socketId = data.socketId;
    this.bullet.isBullet = true;

    // Setup physics
    game.physics.p2.enable(this.bullet);
    this.bullet.body.rotation = data.playerRot;
    this.bullet.body.data.mass = 0.00001;
    this.bullet.body.collideWorldBounds = false;
    this.bullet.outOfBoundsKill = true;
    this.bullet.checkWorldBounds = true;
    this.bullet.body.setCollisionGroup(game.collisionGroups.shotsCG);
    this.bullet.body.collides([game.collisionGroups.wallsCG]);

    // Bullet stats
    this.bullet.dmg = data.weaponData.amount;
    this.bullet.lifespan = data.weaponData.range;
    this.bullet.dmgType = data.weaponData.type;

    // Move bullet to where player is.
    this.bullet.reset(
        data.playerX + (50 * Math.cos(data.playerRot - Phaser.Math.degToRad(90))),
        data.playerY + (50 * Math.sin(data.playerRot - Phaser.Math.degToRad(90)))
    );

    // Fire!
    var speed = 0;
    if (data.bulletVel === undefined) {
        data.bulletVel = 500;
    }
    if (data.bulletVel !== 0) {
        speed = Math.sqrt(data.velX * data.velX + data.velY * data.velY) + 1;
    }
    this.bullet.body.moveForward((speed * 20) + data.bulletVel);

    // Has animation?
    if (data.animation) {
        var animate = this.bullet.animations.add('animate');
        this.bullet.animations.play('animate', 30, true);
    }

}


/**
 * Tell server about this bullet.
 */
Shot.prototype.broadcast = function() {
    socket.emit('gameUpdate', this.data);
};
