/*global
    io, jQuery, Ship, dbShips, selectedShip, dbWeapons, Shot
*/
// [TODO]: Move these gloabls to private after testing.
var game;
var PlayerData = {};    // Object of player ship (from current user and passed from other clients on join), key is socket id.
var Players = {};       // Object of in game ships (from Ships Class), key is socket id.
var ClientSockets = []; // Array of client socket id's.
var selectedArena = 0;
var whoKilledYou = '';

/**
 * The game.
 * @class
 */
function USCG() {
    //'use strict';
    var socket = io();

    // For measuring ping with server
    var pingTime;
    var ping;
    socket.on('pong', function () {
        var latency = Date.now() - pingTime;
        ping = latency;
    });
    function measurePing() {
        setTimeout(function () {
            pingTime = Date.now();
            socket.emit('ping');
            //console.log(ping);
            measurePing();
        }, 2000);
    }
    measurePing();

    // On connection lost.
    socket.on('disconnect', function(data) {
        console.log('connection lost', data);
    });

    // Update player count when client joins room.
    socket.on('playerJoined', function (data) {
        //console.log('playerJoined', data);
        ClientSockets.push(data.socketId);
        PlayerData[data.socketId] = data.shipstuffs;
    });

    // Update player count when client leaves room.
    socket.on('playerLeft', function (data) {
        console.log('playerLeft', data);
        if(Players[data.socketId] != undefined) {
            Players[data.socketId].destroy();
            delete Players[data.socketId];
        }
    });

    socket.on('error', function(data) {
        console.log(data);
        //window.alert(data);
        location.href = location.href.replace(location.hash,'');
    });

    socket.on('errorMsg', function(data) {
        console.log('[Error:'+data.num+'] '+data.msg);
        //window.alert('Error '+data.num+'\n'+data.msg);
        location.href = location.href.replace(location.hash,'');
    });

    game = new Phaser.Game(800, 600, Phaser.AUTO, 'game', null, false, false);

    // Loads the loadbar image into game.
    var BootState = {
        preload: function () {
            this.load.image('loadingBar', 'assets/preload/loadbar.gif');
        },

        create: function () {
            game.state.start('preload');
        }
    };

    // This state loads all the images, sprites, music, or whatever we need in the game.
    var PreloadState = {
        preload: function () {
            //console.log('PreloadState preload')

            // Don't allow pause on lose focus.
            game.stage.disableVisibilityChange = true;

            this.loadingBar = game.add.sprite(0, 0, 'loadingBar');
            this.loadingBar.x = game.world.centerX - this.loadingBar.width / 2;
            this.loadingBar.y = game.world.centerY - this.loadingBar.height / 2;
            game.load.setPreloadSprite(this.loadingBar);

            // Load assets.
            // Ready button
            this.game.load.spritesheet('btn-ready', 'assets/buttons/btn-ready.png', 259, 0);
            this.game.load.spritesheet('btn-notReady', 'assets/buttons/btn-notReady.png', 181, 0);

            // Powerups
            this.game.load.spritesheet('pu-blue', 'assets/powerups/blue.png', 43, 0);
            this.game.load.spritesheet('pu-green', 'assets/powerups/green.png', 43, 0);
            this.game.load.spritesheet('pu-red', 'assets/powerups/red.png', 43, 0);

            // Hud elements (health bar's etc...)
            this.load.image('healthCenter', 'assets/hud/healthCenter.png');
            this.load.image('armorBar', 'assets/hud/armorBar.png');
            this.load.image('shieldBar', 'assets/hud/shieldBar.png');

            var i;
            var j;
            var spritelen;
            // Ships, from ships object taken out of database.
            var shipslen = dbShips.length;
            for(i = 1; i < shipslen; i++) { // First id is always null.
                spritelen = dbShips[i].sprites.length;
                for(j = 0; j < spritelen; j++) {
                    game.load.image(dbShips[i].sprites[j].name, dbShips[i].sprites[j].path);
                }
            }
            game.load.physics('greenShipShapes', 'assets/ships/green/shapes.json'); // should make it a single ship shape file lol


            // Weapons, from weapons object taken out of database.
            var weaponslen = dbWeapons.length;
            for(i = 1; i < weaponslen; i++) { // First id is always null.
                spritelen = dbWeapons[i].sprites.length;
                for(j = 0; j < spritelen; j++) {
                    game.load.image(dbWeapons[i].sprites[j].name, dbWeapons[i].sprites[j].path);
                }
            }

            // Misc sprites
            game.load.spritesheet('a104-mine', 'assets/active/a104-mine.png', 32, 32);

            // Arenas backgrounds
            game.load.image('background1', 'assets/arenas/background1.jpg');
            game.load.image('background2', 'assets/arenas/background2.jpg');
            game.load.image('background3', 'assets/arenas/background3.jpg');
            game.load.image('wall1', 'assets/arenas/wall1.png');
            game.load.image('wall2', 'assets/arenas/wall2.png');
            game.load.image('wall3', 'assets/arenas/wall3.png');
            game.load.image('wall-circle', 'assets/arenas/wall-circle.png');

            // Arena music
            game.load.audio('badass', ['assets/audio/BoxCat_Games_-_23_-_Trace_Route.mp3', 'assets/audio/BoxCat_Games_-_23_-_Trace_Route.ogg']);
        },

        create: function () {
            //console.log('PreloadState create')

            this.btnReady = game.add.button(0, 0, 'btn-ready', this.startSyncState, this, 1, 0, 2);
            this.btnReady.x = game.world.centerX - this.btnReady.width / 2;
            this.btnReady.y = game.world.centerY - this.btnReady.height / 2;

            this.btnNotReady = game.add.button(0, 0, 'btn-notReady', this.returnToShipSelect, this, 1, 0, 2);
            this.btnNotReady.x = game.world.centerX - this.btnNotReady.width / 2;
            this.btnNotReady.y = game.world.centerY - this.btnNotReady.height / 2 + 100;

            // Done loading assets.
            this.loadingBar.destroy();
        },

        // Once preload is complete, player can join in.
        startSyncState: function() {
            this.btnReady.destroy();
            socket.emit('join', selectedShip, function(data) {
                // Update this new joined client with list of clients already in the room (including initil ship data)

                PlayerData = jQuery.extend(true, {}, data.clientsData);
                ClientSockets = Object.keys(PlayerData);
                ClientSockets = jQuery.unique(ClientSockets);
                selectedArena = data.arena;

                game.state.start('sync', false, false, {startPos: data.startPos});
            });
        },

        returnToShipSelect: function() {
            location.href = '/ships';
        }
    };

    // State when waiting on at least one other player.
    var SyncState = {
        roomReady: false,

        init: function (data) {
            var self = this;

            // Don't allow pause on lose focus.
            game.stage.disableVisibilityChange = true;

            // data to pass on.
            self.data = data;

            // Room is ready.
            socket.on('roomStart', function(data, ack) {
                self.roomReady = true;
                ack(socket.id);
            });
        },

        create: function () {
            //console.log('SyncState create')

            var style = {font: '30px Arial', fill: '#ffffff', align: 'center'};
            this.text = game.add.text(game.world.centerX, game.world.centerY, 'Waiting for one other player... ', style);
            this.text.anchor.setTo(0.5, 0.5);
        },

        update: function () {
            //console.log('SyncState update')
            var self = this;

            socket.emit('signalReady', socket.id);

            if (this.roomReady) {
                this.text.destroy();
                game.state.start('game', false, false, self.data);
            }
        }
    };

    // State when a player dies.
    var DeathState = {
        shotsFired: 0,
        shotsHit: 0,

        init: function(sFired,sHit) {
        shotsFired = sFired;
        shotsHit = sHit;
        game.add.sprite(0, 0, 'background1');
        game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
        },
        preload: function() {
        // State preload logic goes here

        },
        create: function(){
        // State create logic goes here

        var style = { font: "32px Lucida Sans Typewriter", fill: "#94ce50", boundsAlignH: "center",
        boundsAlignV: "middle",tabs: [ 220, 150, 200 ] };
        var text = 'Player Game Stats!\n\nKilled by:\t'+whoKilledYou+'\n'+
        'Shots Fired:\t'+shotsFired+''+'\nShots Hit:\t'+shotsHit+'\n\n\nclick anywhere to continue...';

        //  The Text is positioned at 0, 100
        text = game.add.text(0, 0, text, style);
        text.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);

        //  We'll set the bounds to be from x0, y100 and be 800px wide by 100px high
        text.setTextBounds(0, 100, 800, 100);

        game.input.onDown.addOnce(this.onClick, this);


        },
        update: function() {
        // State Update Logic goes here.

        },
        onClick: function() {
        //game.state.start('preload');
        window.location.href = 'game';
        }
    };

    // The main event.
    var GameState = {
        gameRunning: false,
        players: 0,
        currentSpeed: 0,
        socketTiming: 0,
        socketDelay: 100,

        // phaser.io init
        init: function (data) {
            var self = this;

            // Don't allow pause on lose focus.
            game.stage.disableVisibilityChange = true;

            this.startPos = data.startPos;

            // Get opponet clients data from server.
            socket.on('clientUpdate', function(data) {
                self.clientUpdate(data);
            });

            // Get a new opponet clients data from server.
            socket.on('newClientJoin', function(data) {
                self.newClientJoin(data);
            });
        },

        // phaser.io create
        create: function () {
            var self = this;

            game.world.setBounds(0, 0, 2000, 2000);
            game.renderer.roundPixels = true;
            game.physics.startSystem(Phaser.Physics.P2JS);
            game.physics.p2.setImpactEvents(true);
            game.physics.p2.restitution = 0.5;

            // Collision groups
            game.collisionGroups = {};
            game.collisionGroups.shipsCG = game.physics.p2.createCollisionGroup();
            game.collisionGroups.wallsCG = game.physics.p2.createCollisionGroup();
            game.collisionGroups.shotsCG = game.physics.p2.createCollisionGroup();
            game.collisionGroups.enemyShotsCG = game.physics.p2.createCollisionGroup();

            // Setup the same arena for all clients, server chooses.
            game.arena = new Arena(game, selectedArena);

            // Create a ship for everyone in the room (including me)
            for(var i = 0; i < ClientSockets.length; i++) {
                if (ClientSockets[i] !== socket.id) {
                    Players[ClientSockets[i]] = new Ship(game, ClientSockets[i], PlayerData[ClientSockets[i]]);
                } else {
                    Players[ClientSockets[i]] = new Ship(game, ClientSockets[i], PlayerData[ClientSockets[i]], game.arena.startPos[self.startPos]);
                }
            }

            this.gameRunning = true;
        },

        // phaser.io update
        update: function () {

            if (this.gameRunning) {

                // update all ships
                if (Players[socket['id']] != undefined) {
                    Players[socket['id']].update();
                }

                game.arena.update();
            }

            this.updateServer();

        },

        // Return time to contact server.
        getSocketDelay: function() {
            return ping < this.socketDelay ? ping : this.socketDelay;
        },

        // Send my location data to server.
        updateServer: function () {
            this.socketTiming += game.time.elapsed;
            if (this.socketTiming < this.getSocketDelay()) {
                return;
            }

            this.socketTiming = 0;
            if (Players[socket['id']] != undefined) {
                Players[socket['id']].broadcast();
            }
        },

        // When opponet data comes from server, update all opponet ships and shots.
        clientUpdate: function(data) {
            // Ignore if socket id is not found, or if this is from me.
            if (Players[data.socketId] !== undefined && data.socketId !== socket.id) {
                // Player ships are updated reglarly (like 10 times a second)
                if (data.ship) {
                    Players[data.socketId].update(data);

                // Shot data is only sent once per shot.
                } else if (data.shot) {
                    // this is a shot
                    var shot = new Shot(data);
                    shot.bullet.body.setCollisionGroup(game.collisionGroups.enemyShotsCG);
                    shot.bullet.body.collides([game.collisionGroups.shipsCG, game.collisionGroups.wallsCG]);

                } else if (data.shotUpdate) {
                    Players[data.whodidit].roundStats.damageDone += data.damageDone;
                    Players[data.whodidit].roundStats.shotsHit++;
                    //whoKilledYou = data.whodidit;
                    whoKilledYou = Players[data.whodidit].roundStats.gamerTag;

                    // Popup damage for ememies.
                    Players[data.whodidit].dmgTextPopup(data.damageDone, data.playerPos);
                }

            } else {
                // This is a powerup spawn
                if (data.isPowerup) {
                    game.arena.spawnPowerup(data.spawnLoc, data.whichPowerup);
                }
            }
        },

        // What to do when a new client joins in the middle of a match.
        newClientJoin: function(data) {
            ClientSockets.push(data.socketId);
            PlayerData[data.socketId] = data.shipstuffs;
            Players[data.socketId] = new Ship(game, data.socketId, data.shipstuffs);
        },

        // phaser.io render (for debug)
        render: function() {
            //for(var i = 0; i < ClientSockets.length; i++) {
            //    if(Players[ClientSockets[i]] !== undefined) {
            //        //game.debug.body(Players[ClientSockets[i]].player);
            //        //game.debug.spriteBounds(Players[ClientSockets[i]].player);

            //    }
            //}
            //game.debug.cameraInfo(game.camera, 32, 32);
        }

    };

    game.state.add('boot', BootState, true);
    game.state.add('preload', PreloadState, false);
    game.state.add('sync', SyncState, false);
    game.state.add('game', GameState, false);
    game.state.add('death', DeathState, false);

    this.switchToSync = function(data) {
        game.state.start('sync', false, false, data);
    };
    this.switchToDeath = function(data) {
        game.state.start('death', false, false, data);
    };

    this.getSocket = function() {
        return socket;
    };

    return this;
}


/**
 * Return's this player's unique socket it.
 * @return {string}
 */
USCG.prototype.getSocket = function () {
    return this.getSocket();
};


/**
 * Signals the game to switch to sync state.
 */
USCG.prototype.sync = function(data) {
    this.switchToSync(data);
};


/**
 * Signals the game to switch to death state.
 */
USCG.prototype.death = function(data) {
    this.switchToDeath(data);
};


/**
 *  Can call forfeit externally.
 */
USCG.prototype.forfeit = function() {
    var sure = confirm('are you sure?');
    if(sure) {
        socket.emit('forfeit', {socketId: socket.id});
        window.location.href = 'ships';
    }
};
