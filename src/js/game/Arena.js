/*global
    PlayerObj, dbWeapons, dbPassives, dbActives, dbPowerups, Shot
*/

/**
 * Represents a player's ship.
 * @class
 * @param {object} game - The game object this ship belongs to.
 * @param {number} arena - Which arena to render, chosen at random by server when room is created.
 */
function Arena(game, arena) {
    'use strict';

    this.game = game;
    this.arena = arena;

    // background
    this.stars = game.add.tileSprite(0, 0, 2000, 2000, 'background' + this.arena);
    this.stars.fixedToCamera = true;

    // walls
    var walls = this.game.add.group();
    walls.enableBody = true;
    walls.physicsBodyType = Phaser.Physics.P2JS;
    walls.enableBodyDebug = true;

    switch(this.arena) {

        // the cage
        case 1:

            //nw wall
            var wall1 = this.game.add.sprite(15, 450, 'wall1');
            wall1.height = 900;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //north wall
            var wall2 = this.game.add.sprite(1000, 15, 'wall1');
            wall2.height = 30;
            wall2.width = 2000;
            wall2.anchor.setTo(0, 0);
            walls.add(wall2);
            wall2.body.kinematic = true;
            wall2.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall2.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //ne wall
            var wall3 = this.game.add.sprite(1985, 450, 'wall1');
            wall3.height = 900;
            wall3.width = 30;
            wall3.anchor.setTo(0, 0);
            walls.add(wall3);
            wall3.body.kinematic = true;
            wall3.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall3.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //se wall
            var wall4 = this.game.add.sprite(1985, 1550, 'wall1');
            wall4.height = 900;
            wall4.width = 30;
            wall4.anchor.setTo(0, 0);
            walls.add(wall4);
            wall4.body.kinematic = true;
            wall4.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall4.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //south wall
            var wall5 = this.game.add.sprite(1000, 1985, 'wall1');
            wall5.height = 30;
            wall5.width = 2000;
            wall5.anchor.setTo(0, 0);
            walls.add(wall5);
            wall5.body.kinematic = true;
            wall5.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall5.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //sw wall
            var wall6 = this.game.add.sprite(15, 1550, 'wall1');
            wall6.height = 900;
            wall6.width = 30;
            wall6.anchor.setTo(0, 0);
            walls.add(wall6);
            wall6.body.kinematic = true;
            wall6.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall6.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);
            break;

        // the pac
        case 2:
            //top left wall
            var wall1 = game.add.sprite(450, 15, 'wall2');
            wall1.height = 30;
            wall1.width = 900;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //top rigth wall
            wall1 = game.add.sprite(1550, 15, 'wall2');
            wall1.height = 30;
            wall1.width = 900;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //bottom left wall
            wall1 = game.add.sprite(450, 1985, 'wall2');
            wall1.height = 30;
            wall1.width = 900;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //bottom rigth wall
            wall1 = game.add.sprite(1550, 1985, 'wall2');
            wall1.height = 30;
            wall1.width = 900;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // left top wall
            wall1 = this.game.add.sprite(15, 450, 'wall2');
            wall1.height = 900;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // left bottom wall
            wall1 = this.game.add.sprite(15, 1550, 'wall2');
            wall1.height = 900;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // right top wall
            wall1 = this.game.add.sprite(1985, 450, 'wall2');
            wall1.height = 900;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // right bottom wall
            wall1 = this.game.add.sprite(1985, 1550, 'wall2');
            wall1.height = 900;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // top left inset wall
            wall1 = game.add.sprite(365, 235, 'wall2');
            wall1.height = 30;
            wall1.width = 200;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            wall1 = game.add.sprite(235, 365, 'wall2');
            wall1.height = 200;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // top right inset wall
            wall1 = game.add.sprite(1635, 235, 'wall2');
            wall1.height = 30;
            wall1.width = 200;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            wall1 = game.add.sprite(1765, 365, 'wall2');
            wall1.height = 200;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // bottom left inset wall
            wall1 = game.add.sprite(365, 1800, 'wall2');
            wall1.height = 30;
            wall1.width = 200;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            wall1 = game.add.sprite(235, 1650, 'wall2');
            wall1.height = 200;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // middle T
            wall1 = game.add.sprite(1000, 1050, 'wall2');
            wall1.height = 30;
            wall1.width = 350;
            //wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            wall1 = game.add.sprite(1000, 1150, 'wall2');
            wall1.height = 170;
            wall1.width = 30;
            //wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // middle box
            wall1 = game.add.sprite(1000, 900, 'wall2');
            wall1.height = 15;
            wall1.width = 400;
            //wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            wall1 = game.add.sprite(800, 820, 'wall2');
            wall1.height = 150;
            wall1.width = 15;
            //wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            wall1 = game.add.sprite(1200, 820, 'wall2');
            wall1.height = 150;
            wall1.width = 15;
            //wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            break;

        // the classic
        case 3:
            //top left wall
            var wall1 = game.add.sprite(450, 15, 'wall3');
            wall1.height = 30;
            wall1.width = 900;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //top rigth wall
            wall1 = game.add.sprite(1550, 15, 'wall3');
            wall1.height = 30;
            wall1.width = 900;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //bottom left wall
            wall1 = game.add.sprite(450, 1985, 'wall3');
            wall1.height = 30;
            wall1.width = 900;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            //bottom rigth wall
            wall1 = game.add.sprite(1550, 1985, 'wall3');
            wall1.height = 30;
            wall1.width = 900;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // left top wall
            wall1 = this.game.add.sprite(15, 250, 'wall3');
            wall1.height = 500;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // left middle wall
            wall1 = this.game.add.sprite(15, 1000, 'wall3');
            wall1.height = 600;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // left bottom wall
            wall1 = this.game.add.sprite(15, 1750, 'wall3');
            wall1.height = 500;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // right top wall
            wall1 = this.game.add.sprite(1985, 250, 'wall3');
            wall1.height = 500;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // right middle wall
            wall1 = this.game.add.sprite(1985, 1000, 'wall3');
            wall1.height = 600;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // right bottom wall
            wall1 = this.game.add.sprite(1985, 1750, 'wall3');
            wall1.height = 500;
            wall1.width = 30;
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);

            // center circle
            wall1 = this.game.add.sprite(1000, 1000, 'wall-circle');
            wall1.scale.set(15);
            wall1.anchor.setTo(0, 0);
            walls.add(wall1);
            wall1.body.kinematic = true;
            wall1.body.setCircle(187);
            wall1.body.setCollisionGroup(this.game.collisionGroups.wallsCG);
            wall1.body.collides([this.game.collisionGroups.shipsCG, this.game.collisionGroups.shotsCG, this.game.collisionGroups.enemyShotsCG]);
            break;
    }


    // start possitions, this particular array should work for all current arenas.
    this.startPos = [
        {x: 100, y: 100},
        {x: 200, y: 100},
        {x: 300, y: 100},
        {x: 400, y: 100},
        {x: 500, y: 100},
        {x: 600, y: 100},
        {x: 700, y: 100},
        {x: 800, y: 100},
        {x: 900, y: 100},
        {x: 1000, y: 100},
        {x: 1100, y: 100},
        {x: 1200, y: 100},
        {x: 1300, y: 100},
        {x: 1400, y: 100},
        {x: 1500, y: 100},
        {x: 1600, y: 100},
        {x: 1700, y: 100},
        {x: 1800, y: 100},
        {x: 1900, y: 100},

        {x: 100, y: 1900},
        {x: 200, y: 1900},
        {x: 300, y: 1900},
        {x: 400, y: 1900},
        {x: 500, y: 1900},
        {x: 600, y: 1900},
        {x: 700, y: 1900},
        {x: 800, y: 1900},
        {x: 900, y: 1900},
        {x: 1000, y: 1900},
        {x: 1100, y: 1900},
        {x: 1200, y: 1900},
        {x: 1300, y: 1900},
        {x: 1400, y: 1900},
        {x: 1500, y: 1900},
        {x: 1600, y: 1900},
        {x: 1700, y: 1900},
        {x: 1800, y: 1900},
        {x: 1900, y: 1900}
    ];

    // Power ups.
    this.powerupsSpawnLocations = [];

    // Powerup spawn locations are different based on arnea.
    switch(this.arena) {

        // the cage
        case 1:
            // corners
            this.powerupsSpawnLocations.push({x: 150, y: 150});     // top-left
            this.powerupsSpawnLocations.push({x: 150, y: 1850});    // bottom-left
            this.powerupsSpawnLocations.push({x: 1850, y: 150});    // top-right
            this.powerupsSpawnLocations.push({x: 1850, y: 1850});   // bottom-right
            break;

        // the pac
        case 2:
            // corners
            this.powerupsSpawnLocations.push({x: 150, y: 150});     // top-left
            this.powerupsSpawnLocations.push({x: 150, y: 1850});    // bottom-left
            this.powerupsSpawnLocations.push({x: 1850, y: 150});    // top-right
            this.powerupsSpawnLocations.push({x: 1850, y: 1850});   // bottom-right

            this.powerupsSpawnLocations.push({x: 1000, y: 1000});   // middle
            break;

        // the classic
        case 3:
            // corners
            this.powerupsSpawnLocations.push({x: 150, y: 150});     // top-left
            this.powerupsSpawnLocations.push({x: 150, y: 1850});    // bottom-left
            this.powerupsSpawnLocations.push({x: 1850, y: 150});    // top-right
            this.powerupsSpawnLocations.push({x: 1850, y: 1850});   // bottom-right

            // around planet
            this.powerupsSpawnLocations.push({x: 1000, y: 750});    // top
            this.powerupsSpawnLocations.push({x: 1250, y: 1000});   // right
            this.powerupsSpawnLocations.push({x: 1000, y: 1250});   // bottom
            this.powerupsSpawnLocations.push({x: 750, y: 1000});    // left
            break;
    }

    // Start some battle music!
    this.music = game.add.audio('badass');
    this.music.loopFull();

}


/**
 * Called on game update loop.
 */
Arena.prototype.update = function() {
    var self = this;

    this.stars.tilePosition.x = -this.game.camera.x;
    this.stars.tilePosition.y = -this.game.camera.y;

};

/**
 * Havet his arena spawn a powerup.
 * @param {number} spawnLoc - which spawn location (dependant on arena).
 * @param {number} whichPowerup - Which powerup to spawn.
 */
Arena.prototype.spawnPowerup = function (spawnLoc, whichPowerup) {

    // Random power up from db.
    var choosenPowerup = dbPowerups[whichPowerup];

    // Get randome spawn point for this arena.
    var powerupLoc = this.powerupsSpawnLocations[spawnLoc];

    // insert powerup sprite.
    var powerup = this.game.add.sprite(powerupLoc.x, powerupLoc.y, choosenPowerup.sprite.name);
    this.game.physics.p2.enable(powerup);
    powerup.isPowerup = true;
    powerup.effect = choosenPowerup.effect;
    powerup.animations.add('animate');
    powerup.animations.play('animate', 10, true);
};
