// Ship screen tour object.
var ships = new Tour({
    storage: false,
    steps: [
        {
            title: 'Welcome to LiveSpace',
            content: 'This is the Ship Selection page. There is a short tour for each page to explain what everything is.',
            orphan: true
        },
        {
            element: '#music-control',
            placement: 'bottom',
            title: 'Sound Controls',
            content: 'First things first, by default all sounds are off because that\'s how the web should work. However we have some cool music that can be turned on with this control.'
        },
        {
            element: '#mi-ships',
            placement: 'bottom',
            title: 'Change Ship',
            content: 'This button on the navigation bar will always bring you back to the change ship page.'
        },
        {
            element: '#mi-workshop',
            placement: 'bottom',
            title: 'Workshop',
            content: 'This will take you to the workshop where you can customize your selected ship.'
        },
        {
            element: '#mi-store',
            placement: 'bottom',
            title: 'Store',
            content: 'This will take you to the store where you can buy things to customize your ships. You can also buy new ships!'
        },
        {
            element: '.navbar-right',
            placement: 'left',
            title: 'Player Menu',
            content: 'This drop down menu contains informaiton about you. You can see your stats gathered while you play, change your GamerTag on the profile page, and change the defalt keys to fly your ship (you know, if you think ours suck).'
        },
        {
            element: '.selectedShip',
            placement: 'right',
            title: 'Selected Ship',
            content: 'This is the ship currently selected. It will be the ship used when you move on to the workshop or start the game.'
        },
        {
            element: '#mb-play',
            placement: 'right',
            title: 'Get into the Arena',
            content: 'And don\'t forget the most important part... click here to get into the arena!',
            onHide: function() {
                PlayerObj.stats.toursTaken.ships = true;
                savePlayerObj(PlayerObj);
            }
        }
    ]
});


// Workshop screen tour object.
var workshop = new Tour({
    storage: false,
    steps: [
        {
            title: 'Welcome to the Workshop',
            content: 'This is where you customize your ships.',
            orphan: true
        },
        {
            element: '.editShipName',
            placement: 'right',
            title: 'Change Ship Name',
            content: 'Here you can change your ship\'s name. Surly you can come up with something better than Green Frigate.'
        },
        {
            element: '#upgrade-acceleration',
            placement: 'top',
            title: 'Upgrade Attributes',
            content: 'Clicking on the orange plus sign will upgrade your ship attributes. Notice the number next to the attribite, that is the upgrades cost in credits.'
        },
        {
            element: '#ship',
            placement: 'right',
            title: 'Ship',
            content: 'Your ship might change as you upgrade attributes.'
        },
        {
            element: '#tour-stop-acceleration',
            placement: 'bottom',
            title: 'What is Acceleration?',
            content: 'How fast your ship gains speed. The only speed limit is light speed, but some ships get there faster than others.'
        },
        {
            element: '#tour-stop-thrust',
            placement: 'right',
            title: 'What is Thrust?',
            content: 'This represents how good your thrusters are, more power means faster turning, strafing, and reversing.'
        },
        {
            element: '#tour-stop-armor',
            placement: 'top',
            title: 'What is Armor?',
            content: 'The (usually) metallic plating that holds your ship together. Run out of this and you\'re dead.'
        },
        {
            element: '#tour-stop-shield',
            placement: 'top',
            title: 'What is Shield?',
            content: 'Energy that encaspulates your ship from harm. As long as you have sheilds your armor won\'t take damage. Keep in mind some weapons do more damage to shields than to armor.'
        },
        {
            element: '#tour-stop-shieldRegen',
            placement: 'top',
            title: 'What is Shield Regeneration?',
            content: 'But sheilds have a huge advantage over armor, they will regenerate if you can avoid being hit for a few seconds. This will increase how quick your shields are replenished after beign damaged.'
        },
        {
            element: '#tour-stop-slot-weapon',
            placement: 'left',
            title: 'What is a Weapon Slot?',
            content: 'You gotta have weapons if you want to kill someone. Change out this ship\'s weapon here. You can find better ones in the store.'
        },
        {
            element: '#tour-stop-slot-active',
            placement: 'left',
            title: 'What is an Active Module Slot?',
            content: 'Active modules are also found in the store, they can do all kinds of different things (use F by default) from change your direction instantly, give you a quick shield boost, or drop mines.'
        },
        {
            element: '#tour-stop-slot-passive',
            placement: 'left',
            title: 'What is a Passive Module Slot?',
            content: 'Passive modules are similar to active except they are always on. They may not be as powerful as their Active module counterparts, but some ships have more than one passive slot.',
            onHide: function() {
                PlayerObj.stats.toursTaken.workshop = true;
                savePlayerObj(PlayerObj);
            }
        }
    ]
});

// Store screen tour object.
var store = new Tour({
    storage: false,
    steps: [
        {
            title: 'Welcome to the Store',
            content: 'This is where you can buy new weapons and modules to out your ships, or even buy new and better ships.',
            orphan: true
        },
        {
            element: '#panel1',
            placement: 'bottom',
            title: 'Finding Stuff to Buy',
            content: 'Click on this panel to filter what you can see in the store.',
        },
        {
            element: '.buyCell:first',
            placement: 'right',
            title: 'To Buy',
            content: 'Click on the buy button to purchase the item to the right. Sorry no selling back yet.',
        },
        {
            element: '#credits',
            placement: 'top',
            title: 'Credits',
            content: 'Here is your total credits, earn more by playing in the arena.',
            onHide: function() {
                PlayerObj.stats.toursTaken.store = true;
                savePlayerObj(PlayerObj);
            }
        }
    ]
});

// Show tour button on each page if this player's tour flag is still false.
$(document).ready(function() {

    if (typeof PlayerObj !== "undefined") {
        setTimeout(function () {

            // Ships screen tour.
            if (!PlayerObj.stats.toursTaken.ships || PlayerObj.stats.toursTaken.ships === 'false') {

                // Tour button element.
                $tourpopup = $(
                    '<div class="tour-button col-xs-1 col-xs-push-10" id="tour-ships">\
                        New? Click for tour.\
                    </div>');

                // Add tour button to page.
                $tourpopup.appendTo('#shipselect').hide().fadeIn('slow');

                // Start tour and hide button.
                $('body').on('click', '#tour-ships', function() {
                    ships.init();
                    ships.start();
                    $(this).hide('slow');
                });
            }

            // Workshop screen tour.
            if (!PlayerObj.stats.toursTaken.workshop || PlayerObj.stats.toursTaken.workshop === 'false') {

                // Tour button element.
                $tourpopup = $(
                    '<div class="tour-button col-xs-1 col-xs-push-10" id="tour-workshop">\
                        New? Click for tour.\
                    </div>');

                // Add tour button to page.
                $tourpopup.appendTo('#workshop').hide().fadeIn('slow');

                // Start tour and hide button.
                $('body').on('click', '#tour-workshop', function() {
                    workshop.init();
                    workshop.start();
                    $(this).hide('slow');
                });

            }

            if (!PlayerObj.stats.toursTaken.store || PlayerObj.stats.toursTaken.store === 'false') {

                // Tour button element.
                $tourpopup = $(
                    '<div class="tour-button col-xs-1 col-xs-push-10" id="tour-store">\
                        New? Click for tour.\
                    </div>');

                // Add tour button to page.
                $tourpopup.appendTo('#store').hide().fadeIn('slow');

                // Start tour and hide button.
                $('body').on('click', '#tour-store', function() {
                    store.init();
                    store.start();
                    $(this).hide('slow');
                });
            }
        }, 1000);
    }
});
