
/**
 * Makes numbers pretty.
 * @param {number} x - The number to make pretty.
 */
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


/**
 * Saves player object back to the database.
 * @param {object} PlayerObj - The player object to save.
 * @param {function} callback - The player object to save.
 */
function savePlayerObj(PlayerObj, callback) {
    $.ajax({
        method: 'POST',
        url: '/savePlayerObj',
        data: PlayerObj,
        error: function (data) {
            console.log('error');
            console.log(JSON.stringify(data));
        },
        complete: function () {
            callback();
        }
    });
}


/*
 * Set some page icons to randomly spin between 15 and 75 seconds.
 */
(function loop() {

    // Determine random interval.
    var rand = Math.round(Math.random() * (60000)) + 15000;
    setTimeout(function() {

        // Only do this on pages with a spinning logo header.
        if ($('.page-header .fa-stack').length > 0) {

            // Add spin class.
            $('.page-header .fa-stack').addClass('fa-spin');

            // Update player stats.
            PlayerObj.stats.spinsSeen++;
            savePlayerObj(PlayerObj);

            // Set time out to remove spin when finished.
            setTimeout(function() {
                $('.page-header .fa-stack').removeClass('fa-spin');
            }, 2000);

            // Keep going.
            loop();
        }
    }, rand);
}());
